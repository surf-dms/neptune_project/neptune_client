# Introduction

A data flow is a representation of how data "flows" within one system or through multiple systems. Each flow has multiple steps. Each step is an operation performed on data, such as a copy, a replication, a download, a query and so on.  
Sometimes, the operation affects only the metadata and not the related data.

This is an example of a data flow supported by Neptune:

[shopping cart data flow](data_flow-shopping_cart.md)  


