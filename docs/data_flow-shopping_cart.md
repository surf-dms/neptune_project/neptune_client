## Shopping cart data flow

![shopping cart data flow](img/shoppin-cart_data-flow.png "Shopping cart data flow")

The above steps are implemented in this way:
1. share the metadata
```shell
surfie --conf config.data.provider1.ini session share --noaccess --with "web_portal_provider" --meta "json::/my-data-set-folder/metadata.json" data-provider1::/my-data-set-folder/my-data-set-A
```
2. discover the data: this is implemented within a web portal or other tool. Neptune allows to list the shared data, but not in a friendly way:
```shell
surfie --conf config.web.portal.ini session list --v --type share
sessions:
b9dcc2bc-520a-4c22-9f1b-95f15f57ebe6 active shared 2024-07-24 17:24:48.579000 data_provider1_service_acc
└── share /my-data-set-folder/my-data-set-A from provider data-provider1
```
3. request access to the data
```shell
surfie --conf config.web.portal.ini session request --with "data_provider1_service_acc,data_reviewer" --by "myIdentityProvider::alice@myhost.org" --bymeta "json::user_meta.json" data-provider1::/my-data-set-folder/my-data-set-A/data-object1 data-provider1::/my-data-set-folder/my-data-set-A/data-object3
```
4. approve or reject the request
```shell
surfie --conf config.reviewer.ini session list --v --type request --approve
```
5. share the data
```shell
surfie --conf config.data.provider1.ini session --text approved reshare --type request --from data-provider1 --to data-provider2 --with "data_provider2_service_acc"
```
6. copy or download the data  
```shell
surfie --conf config.data.provider2.ini session download -pt
```
7. acknowledge the successful copy:
```commandline
surfie --conf config.data.provider1.ini acknowledge
```
