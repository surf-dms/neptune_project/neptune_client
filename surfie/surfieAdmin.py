import sys
import json
from enum import Enum

import click
import typer
from pathlib import Path
from typing import Optional
from typing import List, Tuple
from datetime import date, datetime, timedelta

from rich.tree import Tree
from rich import print
from typing_extensions import Annotated


admin_app = typer.Typer()
provider_app = typer.Typer()
admin_app.add_typer(provider_app, name="provider")
datauser_app = typer.Typer()
admin_app.add_typer(datauser_app, name="datauser")
project_app = typer.Typer()
admin_app.add_typer(project_app, name="project")


class ProviderType(str, Enum):
    webp = "web portal"
    irods = "irods"
    davrods = 'davrods'
    yoda = 'yoda web portal'

class DataUserType(str, Enum):
    user = "user"
    admin = "admin"
    provider = "provider"
    reviewer = "reviewer"

class ProjectAttributeType(str, Enum):
    members = "members"
    providers = "providers"
    service_accounts = "service_accounts"
    sram_api_key = "sram_api_key"
    sram_administrators = "sram_administrators"
    sram_logo = "sram_logo"
    sram_sender = "sram_sender"


@provider_app.command("add")
def add_provider(ctx: typer.Context,
                 name: Annotated[str, typer.Argument(help="friendly service name")],
                 defaulttype: Annotated[ProviderType, typer.Option(case_sensitive=False)],
                 endpoints: Annotated[str, typer.Option(help='endpoints list: "type::<fully qualified hostname>:<port>,'
                                                             'type::<fully qualified hostname>:<port>"')] = None,
                 admins: Annotated[str, typer.Option(help='admins list: "admin1,admin2,admin3"')] = None
                 ):

    neptune = ctx.obj.neptune
    eps = []
    administrators = admins.split(',') if admins else []

    if endpoints:
        endpoint_list = endpoints.split(",")
        for endpoint in endpoint_list:
            ep_type, host_port = endpoint.split("::")
            if host_port:
                hostname, port = host_port.split(":")
            else:
                typer.echo("incorrect endpoint: missing hostname and port")
                typer.Exit(0)

            zone_name = None
            if ep_type:
                if ep_type == "irods":
                    zone_name = typer.prompt("You want to add an iRODS endpoint to {}, "
                                             "please specify the zone name".format(name))
                    protocol = "irods"
                elif ep_type == "davrods":
                    protocol = "webdav"
                    zone_name = typer.prompt("You want to add a DAVRODS endpoint to {}, "
                                             "please specify the zone name".format(name))
                else:
                    protocols = click.Choice(['webdav', 'http', 'https', 'ssh'])
                    protocol: click.Choice = typer.prompt("Please, choose the protocol type", show_choices=True,
                                                          type=protocols)
            else:
                typer.echo("incorrect endpoint: missing endpoint type")
                typer.Exit(0)

            eps.append(neptune.create_endpoint(hostname=hostname, port=int(port), protocol=protocol,
                                               zone=zone_name, ep_type=ep_type))

    provider_db = neptune.create_provider(friendly_name=name, endpoints=eps, endpoint_default_type=defaulttype,
                                          administrators=administrators)
    if provider_db:        
        typer.echo("created provider: {}".format(provider_db.friendly_name))


@provider_app.command("list")
def list_provider(ctx: typer.Context,
                  name: Optional[str] = typer.Option(None, "--name", "-n", help="friendly service name"),
                  js: Annotated[bool, typer.Option("--json", help="print json output")] = None):
    neptune = ctx.obj.neptune
    providers_db = [neptune.get_provider_by_name(friendly_name=name)] if name else neptune.providers
    if providers_db:
        if not js:
            typer.echo("Providers:")
        for provider_db in providers_db:
            if js:
                typer.echo(json.dumps(provider_db.to_dict(), indent=4, sort_keys=True, default=str))
            else:
                typer.echo("        {}".format(provider_db.friendly_name))
                for endpoint in provider_db.endpoints:
                    typer.echo("          {} {} {}".format(endpoint.actual_instance.hostname,
                                                            endpoint.actual_instance.port,
                                                            endpoint.actual_instance.protocol))
    else:
        typer.echo("No provider found")


@provider_app.command("remove")
def remove_provider(ctx: typer.Context, name: Optional[str] = typer.Option(None, "--name", "-n",
                                                                           help="friendly service name")):
    neptune = ctx.obj.neptune
    neptune.remove_providers(name)
    if name:
        typer.echo("Removed {}".format(name))
    else:
        typer.echo("Removed all providers")


@project_app.command("add")
def add_project(ctx: typer.Context,
                name: Annotated[str, typer.Argument(help="project name")],
                exp: Annotated[int, typer.Argument(help="time in days to the project's expiration")],
                desc: Annotated[str, typer.Option("--d", help="project's description")] = None
               ):

    delta = timedelta(exp)
    offset = date.today() + delta
    exp_date = datetime(offset.year, offset.month, offset.day)
    neptune = ctx.obj.neptune
    project_db = neptune.create_project(name=name,expiration=exp_date,description=desc)
    if project_db:
        typer.echo("Created project: {}".format(name))


@project_app.command("list")
def list_project(ctx: typer.Context,
                 name: Optional[str] = typer.Option(None, "--name", "-n", help="project name"),
                 verbose: Annotated[bool, typer.Option("--v", help="verbose output")] = None,
                 js: Annotated[bool, typer.Option("--json", help="print json output")] = None):
    neptune = ctx.obj.neptune
    projects_db = neptune.list_projects(name=name)
    if projects_db:
        if not js:
            typer.echo("Projects:")
        for project_db in projects_db:
            if js:
                typer.echo(json.dumps(project_db.to_dict(), indent=4, sort_keys=True, default=str))
            else:
                if verbose:
                    project_tree = Tree("[bold]{}".format(project_db.name))
                    for key, value in project_db.dict().items():
                        if key != 'name':
                            project_tree.add("{}: {}".format(key, value))
                else:
                    project_tree = Tree(
                        "[bold]{} - expires on {}".format(project_db.name, str(project_db.expiration_ts)))
                    if project_db.description:
                        desc_tree = project_tree.add("{}".format(project_db.description))
                print(project_tree)
    else:
        typer.echo("No project found")


@project_app.command("set")
def set_project(ctx: typer.Context,
                name: Annotated[str, typer.Argument(help="project name")],
                attr: Annotated[Tuple[ProjectAttributeType, str],
                                typer.Argument(help="project's attribute to set")] = None):
    neptune = ctx.obj.neptune
    projects_db = neptune.list_projects(name=name)
    if projects_db and len(projects_db)>0:
        project_dict = projects_db[0].dict()
        if attr[0] == ProjectAttributeType.members:
            member_list = neptune.get_data_users_id(data_usernames=attr[1].split(','))
            if not member_list:
                typer.echo("Members' names not found")
                sys.exit(1)
            project_dict[attr[0]] = member_list
        elif attr[0] == ProjectAttributeType.providers:
            provider_list = []
            for provider_name in attr[1].split(','):
                provider_id = neptune.get_provider_id(friendly_name=provider_name)
                if not provider_id:
                    typer.echo("Provider name not found: {}".format(provider_name))
                    sys.exit(1)
                provider_list.append(provider_id)
            project_dict[attr[0]] = provider_list
        elif attr[0] == ProjectAttributeType.service_accounts:
            service_account_dict = {}
            for service_account_pair in attr[1].split(','):
                member, provider = service_account_pair.split('::')
                member_list = neptune.get_data_users_id(data_usernames=[member])
                if member_list and len(member_list)>0:
                    member_id = member_list[0]
                else:
                    typer.echo("Member's name not found: {}".format(member))
                    sys.exit(1)
                provider_id = neptune.get_provider_id(friendly_name=provider)
                if not provider_id:
                    typer.echo("Provider's name not found: {}".format(provider))
                    sys.exit(1)
                service_account_dict[member_id] = provider_id
                project_dict[ProjectAttributeType.members].append(member_id)
                project_dict[ProjectAttributeType.providers].append(provider_id)
                project_dict[ProjectAttributeType.members] = list(set(project_dict[ProjectAttributeType.members]))
                project_dict[ProjectAttributeType.providers] = list(set(project_dict[ProjectAttributeType.providers]))
            project_dict[attr[0]] = service_account_dict
        else:
            if attr[0] == ProjectAttributeType.sram_api_key:
                project_dict['sram_enabled'] = True
            project_dict[attr[0]] = attr[1]
        neptune.update_project(project_id=projects_db[0].id, project_dict=project_dict)
    else:
        typer.echo("No project found")


@project_app.command("set")
def set_project(ctx: typer.Context,
                name: Annotated[str, typer.Argument(help="project name")],
                attr: Annotated[Tuple[ProjectAttributeType, str],
                                typer.Argument(help="project's attribute to set")] = None):
    neptune = ctx.obj.neptune
    projects_db = neptune.list_projects(name=name)
    if projects_db and len(projects_db)>0:
        project_dict = projects_db[0].dict()
        if attr[0] == ProjectAttributeType.members:
            member_list = neptune.get_data_users_id(data_usernames=attr[1].split(','))
            if not member_list:
                typer.echo("Members' names not found")
                sys.exit(1)
            project_dict[attr[0]] = member_list
        elif attr[0] == ProjectAttributeType.providers:
            provider_list = []
            for provider_name in attr[1].split(','):
                provider_id = neptune.get_provider_id(friendly_name=provider_name)
                if not provider_id:
                    typer.echo("Provider name not found: {}".format(provider_name))
                    sys.exit(1)
                provider_list.append(provider_id)
            project_dict[attr[0]] = provider_list
        elif attr[0] == ProjectAttributeType.service_accounts:
            service_account_dict = {}
            for service_account_pair in attr[1].split(','):
                member, provider = service_account_pair.split('::')
                member_list = neptune.get_data_users_id(data_usernames=[member])
                if member_list and len(member_list)>0:
                    member_id = member_list[0]
                else:
                    typer.echo("Member's name not found: {}".format(member))
                    sys.exit(1)
                provider_id = neptune.get_provider_id(friendly_name=provider)
                if not provider_id:
                    typer.echo("Provider's name not found: {}".format(provider))
                    sys.exit(1)
                service_account_dict[member_id] = provider_id
                project_dict[ProjectAttributeType.members].append(member_id)
                project_dict[ProjectAttributeType.providers].append(provider_id)
                project_dict[ProjectAttributeType.members] = list(set(project_dict[ProjectAttributeType.members]))
                project_dict[ProjectAttributeType.providers] = list(set(project_dict[ProjectAttributeType.providers]))
            project_dict[attr[0]] = service_account_dict
        else:
            if attr[0] == ProjectAttributeType.sram_api_key:
                project_dict['sram_enabled'] = True
            project_dict[attr[0]] = attr[1]
        neptune.update_project(project_id=projects_db[0].id, project_dict=project_dict)
    else:
        typer.echo("No project found")


@project_app.command("remove")
def remove_project(ctx: typer.Context, name: Optional[str] = typer.Option(None, "--name", "-n",
                                                                           help="project name")):
    neptune = ctx.obj.neptune
    removed_number = neptune.remove_projects(name)
    if name and removed_number == 1:
        typer.echo("Removed {}".format(name))
    else:
        typer.echo("Removed {} projects".format(removed_number))


@datauser_app.command("add")
def add_datauser(ctx: typer.Context,
                 username: Annotated[str, typer.Argument(help="user name")],
                 password: Annotated[str, typer.Argument(help="password")],
                 role: Annotated[DataUserType, typer.Option(case_sensitive=False)],
                 provider_friendly_name: Annotated[str, typer.Option("--fn", help="provider friendly name")] = None
                 ):
    neptune = ctx.obj.neptune
    friendly_name = neptune.friendly_name
    if provider_friendly_name:
        friendly_name = provider_friendly_name
    data_user_db = neptune.create_data_user(username, password, role, friendly_name)
    if data_user_db:
        typer.echo("Created data user: {} for provider {}".format(data_user_db['username'], friendly_name))


@datauser_app.command("list")
def list_data_user(ctx: typer.Context,
                   provider: Optional[str] = typer.Option(None, "--provider", "-p", help="friendly service name"),
                   js: Annotated[bool, typer.Option("--json", help="print json output")] = None):
    neptune = ctx.obj.neptune
    providers_db = [neptune.get_provider_by_name(friendly_name=provider)] if provider else neptune.providers
    if not js:
        typer.echo("Data users:")
    data_users_dict = {}
    for provider_db in providers_db:
        if not js:
            typer.echo("        {}".format(provider_db.friendly_name))
        data_users_db = neptune.list_data_user(provider_db.friendly_name)
        data_users_dict[provider_db.friendly_name] = []
        for data_user in data_users_db:
            data_users_dict[provider_db.friendly_name].append(data_user.to_dict())
            if not js:
                typer.echo("          {} {} {}".format(data_user.username,
                                                       data_user.role,
                                                       data_user.create_ts))

    if js:
        typer.echo(json.dumps(data_users_dict, indent=4, sort_keys=True, default=str))


@datauser_app.command("remove")
def remove_data_user(ctx: typer.Context,
                    username: Annotated[str, typer.Argument(help="user name")],
                    provider_friendly_name: Annotated[str, typer.Argument(help="provider friendly name")] = None
                    ):
    neptune = ctx.obj.neptune
    friendly_name = neptune.friendly_name
    if provider_friendly_name:
        friendly_name = provider_friendly_name
    msg = neptune.remove_data_users(username, friendly_name)
    typer.echo(msg)

