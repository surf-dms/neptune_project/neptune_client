import logging
import typer
import json

from openapi_client.exceptions import ApiException
from openapi_client.configuration import Configuration
from openapi_client.api_client import ApiClient
from openapi_client.api import DefaultApi, ProjectsApi, ProvidersApi, UsersApi, DataUsersApi, DataApi, MetadataApi
from openapi_client.api import RelationshipApi, SessionApi
from openapi_client.models import Endpoint, Endpoint1, IrodsEndpoint
from openapi_client.models import Event, Profile, ProviderCreate, DataUserCreate, SessionBase, ProjectCreate
from openapi_client.models.provenance_action_type import ProvenanceActionType


class Neptune:
    log = logging.getLogger(__name__)

    def __init__(self, username=None, password=None):
        self.host = None
        self.username = username
        self.passwd = password
        self.api_client = None
        self.configuration = Configuration()
        self.friendly_name = None
        self.irods_friendly_name = "lighthouse"
        self.providers = None

    def set_ssl_params(self, verify_ssl, ssl_ca_cert):
        self.configuration.verify_ssl = verify_ssl
        self.configuration.ssl_ca_cert = ssl_ca_cert

    def set_configuration(self):
        self.configuration.host = self.host
        self.configuration.username = self.username
        self.configuration.password = self.passwd
        self.log.debug('Neptune configuration: {}, {}, {}'.format(self.configuration.username,
                                                                  self.configuration.password,
                                                                  self.configuration.host))

    def set_api_client(self):
        self.api_client = ApiClient(self.configuration)

    def set_log(self, log):
        self.log = log

    def get_username(self):
        return self.username

    def get_provider_by_address(self, hostname, port):
        for provider in self.providers:
            for ep in provider.endpoints:
                if ep.actual_instance.hostname == hostname and ep.actual_instance.port == int(port):
                    return provider
        return None

    def get_provider_by_name(self, friendly_name):
        for provider in self.providers:
            if provider.friendly_name == friendly_name:
                return provider
        return None

    def get_user_id(self):
        with self.api_client as client:
            api_instance = DefaultApi(client)
            try:
                self.log.debug("getting id for user {}".format(client.configuration.username))
                api_response = api_instance.read_current_user_users_me_get()
                return api_response.id
            except ApiException as e:
                print("Exception when calling DefaultApi->read_current_user_users_me_get: %s\n" % e)

    def get_user(self, user_id):
        with self.api_client as client:
            api_instance = DataUsersApi(client)
            try:
                user = None
                self.log.debug("getting user for id {}".format(user_id))
                api_response = api_instance.list_data_users_data_users_get(id=user_id)
                if api_response:
                    user = api_response[0]
                return user
            except ApiException as e:
                print("Exception when calling DataUsersApi->list_data_users_data_users_get: %s\n" % e)

    def get_provider_id(self, friendly_name):
        with self.api_client as client:
            api_instance = ProvidersApi(client)
            try:
                providers_db = api_instance.list_providers_providers_get(friendly_name=friendly_name)
                provider_id = None
                if providers_db and len(providers_db) > 0 and friendly_name:
                    provider_id = providers_db[0].id
                return provider_id
            except ApiException as e:
                print("Exception when calling ProvidersApi->api_instance.list_providers_providers_get: %s\n" % e)

    def get_data_users_id(self, data_usernames):
        with self.api_client as client:
            api_instance = DataUsersApi(client)
            try:
                data_users_id = []
                provider_id = self.get_provider_id(self.friendly_name)
                if not provider_id:
                    return data_users_id
                for username in data_usernames:
                    username = username.strip()
                    if len(username) > 0:
                        self.log.debug("getting id for user {}".format(username))
                        data_users_db = api_instance.list_data_users_data_users_get(username=username,
                                                                                    provider_id=provider_id)
                        if data_users_db and len(data_users_db) > 0:
                            data_users_id.append(data_users_db[0].id)
                return data_users_id
            except ApiException as e:
                print("Exception when calling DataUsersApi->list_data_users_data_users_get: %s\n" % e)

    def get_sessions(self, code=None, text_search=None, json_search=None, status=None, access=None,
                     time_interval=None, author=None):
        with self.api_client as client:
            api_instance = SessionApi(client)
            try:
                if code:
                    code = str(code)
                session_db_list = api_instance.list_session_session_get(code=code, text=text_search,
                                                                        json_search=json_search,
                                                                        status=status, access=access,
                                                                        time_interval=time_interval,
                                                                        author=author)
                return session_db_list
            except ApiException as e:
                print("Exception when calling SessionApi->: list_session_session_get%s\n" % e)

    def create_session(self, access='private', participants=[], lifetime='600'):
        session_dict = {'access': access, 'lifetime': lifetime, 'participants': participants}
        self.log.debug("creating session: {}".format(session_dict))
        return SessionBase.from_dict(session_dict)

    def finalize_session(self, session_base):
        with self.api_client as client:
            api_instance = SessionApi(client)
            try:
                session_db = api_instance.create_session_session_post(session_base)
                return session_db
            except ApiException as e:
                typer.echo("Exception when calling SessionApi->create_session_session_post: %s\n" % e)

    def update_session(self, session, session_id):
        with self.api_client as client:
            api_instance = SessionApi(client)
            try:
                unique_sets = set(frozenset(d.items()) for d in session['events'])
                session['events'] = [dict(s) for s in unique_sets]
                events = []
                for event in session['events']:
                    events.append(Event.from_dict(event))
                self.log.debug("updating session events: {}".format(events))
                session_db = api_instance.update_session_events_session_id_events_patch(id=session_id, event=events)
            except ApiException as e:
                typer.echo("Exception when calling SessionApi->update_session_events_session_id_events_patch: %s\n" % e)
                return None
            try:
                profiles = []
                for profile in session['profiles']:
                    profiles.append(Profile.from_dict(profile))
                self.log.debug("updating session profiles: {}".format(profiles))
                session_db = api_instance.update_session_profile_session_id_profiles_patch(id=session_id,
                                                                                           profile=profiles)
                return session_db
            except ApiException as e:
                typer.echo(
                    "Exception when calling SessionApi->update_session_profile_session_id_profiles_patch: %s\n" % e)

    def create_endpoint(self, hostname, port, protocol, zone=None, ep_type='unknown'):
        self.log.debug("creating endpoint")
        if ep_type in ['irods', 'davrods', 'yoda web portal']:
            endpoint = IrodsEndpoint(hostname=hostname,
                                     port=port,
                                     protocol=protocol,
                                     ep_type=ep_type,
                                     zone=zone)
        else:
            endpoint = Endpoint(hostname=hostname,
                                port=port,
                                protocol=protocol,
                                ep_type=ep_type)
        self.log.debug("endpoint created: {}".format(endpoint.to_dict()))
        return (Endpoint1.from_dict(endpoint.to_dict())).actual_instance.to_dict()

    def get_endpoint(self, provider, endpoint_type=None):
        if endpoint_type:
            ep_type = endpoint_type
        else:
            ep_type = provider.endpoint_default_type
        for endpoint in provider.endpoints:
            if endpoint.actual_instance.ep_type == ep_type:
                return endpoint
        return None

    def add_profile_from_provider(self, session, provider, secret=None, noaccess=None, endpoint_type=None,
                                  username=None, metadata=None):
        self.log.debug("add profile from provider {}".format(provider.friendly_name))
        profile_tag = None
        profile_md = {"user": metadata['user']} if metadata else {}
        self.log.debug("profile metadata: {}".format(profile_md))
        endpoint = self.get_endpoint(provider=provider, endpoint_type=endpoint_type)
        if endpoint:
            ep_key = endpoint.actual_instance.hostname + ":" + str(endpoint.actual_instance.port)
        else:
            self.log.debug("no endpoint found for endpoint_type {}".format(endpoint_type if endpoint_type else 'None'))
            return profile_tag

        if noaccess:
            user_tag = username if username else 'noaccess'
            profile_tag = ep_key + "::" + user_tag + "::" + 'native'
            profile = Profile(tag=profile_tag,
                              secret_type='password',
                              auth_scheme='native',
                              endpoint=Endpoint1.from_dict(endpoint.to_dict()),
                              metadata=profile_md)
            if username:
                profile.username = username
        else:
            if session.access in ["public", "shared"]:
                user_tag = username if username else 'anonymous'
                profile_tag = ep_key + "::" + user_tag + "::" + 'native'
                profile = Profile(tag=profile_tag,
                                  username=user_tag,
                                  secret=secret,
                                  secret_type='ticket',
                                  auth_scheme='native',
                                  endpoint=Endpoint1.from_dict(endpoint.to_dict()),
                                  metadata=profile_md)
            else:
                self.log.debug("adding a profile with private access credentials from a provider is not supported")
                return profile_tag, None
        if session.profiles and len(session.profiles) > 0:
            if not (profile_tag in [p.tag for p in session.profiles]):
                session.profiles.append(profile)
        else:
            session.profiles = [profile]
        self.log.debug("the tag for the new profile is {}".format(profile_tag))
        return profile_tag, session

    def update_events(self, events, session_id):
        with self.api_client as client:
            api_instance = SessionApi(client)
            try:
                self.log.debug("updating session events: {}".format(events))
                session_db = api_instance.update_session_events_session_id_events_patch(id=session_id, event=events)
                return session_db
            except ApiException as e:
                typer.echo("Exception when calling SessionApi->update_session_events_session_id_events_patch: %s\n" % e)

    def add_event(self, session, operation, path, profile_tags, target_path=None, ticket=None, metadata=None):
        event = Event(operation=operation,
                      path=path,
                      profile_tags=profile_tags)
        if target_path:
            event.target_path = target_path
        if ticket:
            event.ticket = ticket
        if metadata:
            event.metadata = metadata
        self.log.debug("Event {}".format(event.to_json()))
        if session.events and len(session.events) > 0:
            session.events.append(event)
        else:
            session.events = [event]
        return session

    def add_signature(self, session, action, rel_sess_code=None):
        with self.api_client as client:
            api_instance = SessionApi(client)
            try:
                self.log.debug("Adding signature {} for session {}".format(action, session.id))
                session_db = api_instance.add_session_signature_session_id_provenance_post(id=session.id, action=action,
                                                                                           related_session=rel_sess_code)
            except ApiException as e:
                typer.echo(
                    "Exception when calling SessionApi->add_session_signature_session_id_provenance_post: %s\n" % e)

    def create_provider(self, friendly_name, endpoints, endpoint_default_type, administrators):
        with self.api_client as client:
            api_instance = ProvidersApi(client)
            try:
                self.log.debug("creating provider")
                provider_create = ProviderCreate.from_dict({"friendly_name": friendly_name,
                                                            "endpoints": endpoints,
                                                            "endpoint_default_type": endpoint_default_type,
                                                            "administrators": administrators})
                provider_db = api_instance.create_provider_providers_post(provider_create)
                return provider_db
            except ApiException as e:
                typer.echo("Exception when calling ProvidersApi->create_provider_providers_post: %s\n" % e)

    def list_providers(self, friendly_name=None, hostname=None, port=None):
        with self.api_client as client:
            api_instance = ProvidersApi(client)
            try:
                self.log.debug("Listing providers with friendly name {}, hostname {}, port {}".format(
                    friendly_name, hostname, str(port)))
                providers_db = api_instance.list_providers_providers_get(friendly_name=friendly_name,
                                                                         hostname=hostname, port=port)
                self.log.debug("Found {} providers".format(len(providers_db)))
                return providers_db
            except ApiException as e:
                typer.echo("Exception when calling ProvidersApi->list_providers_providers_get: %s\n" % e)

    def remove_providers(self, friendly_name=None):
        with self.api_client as client:
            api_instance = ProvidersApi(client)
            try:
                self.log.debug("removing providers")
                providers_db = api_instance.list_providers_providers_get(friendly_name=friendly_name)
                provider_id = None
                if providers_db and len(providers_db) > 0 and friendly_name:
                    provider_id = providers_db[0].id
                    api_instance.delete_providers_providers_delete(provider_id)
                self.log.debug("removing all providers is temporarily disabled because it disrupts authentication")
            except ApiException as e:
                typer.echo("Exception when calling ProvidersApi->delete_providers_providers_delete: %s\n" % e)

    def create_project(self, name, expiration, description):
        with self.api_client as client:
            api_instance = ProjectsApi(client)
            try:
                self.log.debug("creating project")
                project_create = ProjectCreate.from_dict({"name": name,
                                                          "expiration_ts": expiration,
                                                          "description": description})
                project_db = api_instance.create_project_projects_post(project_create)
                return project_db
            except ApiException as e:
                typer.echo("Exception when calling ProjectsApi->create_project_projects_post: %s\n" % e)

    def update_project(self, project_dict, project_id):
        with self.api_client as client:
            api_instance = ProjectsApi(client)
            try:
                self.log.debug("updating project {}: {}".format(project_id, project_dict))
                project = ProjectCreate.from_dict(project_dict)
                project_db = api_instance.update_project_projects_id_patch(id=project_id, project_create=project)
                return project_db
            except ApiException as e:
                typer.echo("Exception when calling ProjectsApi->update_project_projects_id_patch: %s\n" % e)

    def list_projects(self, name=None):
        with self.api_client as client:
            api_instance = ProjectsApi(client)
            try:
                self.log.debug("Listing projects with name {}".format(name))
                projects_db = api_instance.list_projects_projects_get(name=name)
                self.log.debug("Found {} providers".format(len(projects_db)))
                return projects_db
            except ApiException as e:
                typer.echo("Exception when calling ProjectsApi->list_projects_projects_get: %s\n" % e)

    def remove_projects(self, name=None):
        with self.api_client as client:
            api_instance = ProjectsApi(client)
            try:
                self.log.debug("removing projects")
                projects_db = api_instance.list_projects_projects_get(name=name)
                project_id = None
                if projects_db and len(projects_db) > 0:
                    projects_number = len(projects_db)
                    if name:
                        project_id = projects_db[0].id
                        projects_number = 1
                    api_instance.delete_projects_projects_delete(project_id)
                    return projects_number
                else:
                    self.log.debug("Nothing to remove")
                    return 0
            except ApiException as e:
                typer.echo("Exception when calling ProjectsApi->delete_projects_projects_delete: %s\n" % e)

    def create_data_user(self, username, password, role, provider_friendly_name):
        with self.api_client as client:
            api_instance = DataUsersApi(client)
            try:
                self.log.debug("creating data_user")
                username_source = 'local'
                provider_id = self.get_provider_id(provider_friendly_name)
                if not provider_id:
                    self.log.debug("No provider {} found".format(provider_friendly_name))
                    return None
                data_user_create = DataUserCreate.from_dict({"username": username,
                                                             "username_source": username_source,
                                                             "password": password,
                                                             "role": role,
                                                             "provider_id": provider_id})
                data_user_db = api_instance.create_data_user_data_users_post(data_user_create)
                return data_user_db
            except ApiException as e:
                typer.echo("Exception when calling DataUsersApi->create_data_user_data_users_post: %s\n" % e)

    def list_data_user(self, provider_friendly_name):
        with self.api_client as client:
            api_instance = DataUsersApi(client)
            try:
                self.log.debug("listing data users")
                provider_id = self.get_provider_id(provider_friendly_name)
                if not provider_id:
                    self.log.debug("No provider {} found".format(provider_friendly_name))
                    return []
                else:
                    self.log.debug("Provider id {} found".format(provider_id))
                    data_users_db = api_instance.list_data_users_data_users_get(provider_id=provider_id)
                    self.log.debug("Data users: {}".format(data_users_db))
            except ApiException as e:
                typer.echo("Exception when calling DataUsersApi->list_data_users_data_users_get: %s\n" % e)
        return data_users_db

    def remove_data_users(self, username, provider_friendly_name):
        with self.api_client as client:
            api_instance = DataUsersApi(client)
            try:
                self.log.debug("removing data users")
                provider_id = self.get_provider_id(provider_friendly_name)
                if not provider_id:
                    msg = 'No provider {} found'.format(provider_friendly_name)
                    self.log.debug(msg)
                    return msg
                data_users_id = self.get_data_users_id([username])
                if not data_users_id or len(data_users_id) == 0:
                    msg = 'No data user {} found'.format(username)
                    self.log.debug(msg)
                    return msg
                api_instance.delete_data_user_data_users_delete(provider_id=provider_id, id=data_users_id[0])
                msg = 'data user {} deleted from provider {}'.format(username, provider_friendly_name)
                self.log.debug(msg)
                return msg
            except ApiException as e:
                typer.echo("Exception when calling DataUsersApi->delete_data_user_data_users_delete: %s\n" % e)

    def get_provider_from_profile_tag(self, event, tag):
        hostname, port = event.profile_tags[tag].split("::")[0].split(":")
        provider_db = self.get_provider_by_address(hostname=hostname, port=int(port))
        return provider_db

    # extract events as triplets (event, source data provider, target data provider)
    # filtered based on operation, source and target providers
    def get_session_events(self, session, operation=None, source=None, target=None):
        event_list = []
        for event in session.events:
            event_triplet = [event]
            if operation and event.operation != operation:
                continue
            provider_db = self.get_provider_from_profile_tag(event=event, tag="path")
            if not provider_db:
                continue
            if source and source != provider_db.friendly_name:
                continue
            event_triplet.append(provider_db.friendly_name)
            if event.target_path:
                target_provider_db = self.get_provider_from_profile_tag(event=event, tag="target_path")
                if target:
                    if not target_provider_db:
                        continue
                    elif target != target_provider_db.friendly_name:
                        continue
                event_triplet.append(target_provider_db.friendly_name)
            else:
                event_triplet.append(None)
            event_list.append(event_triplet)
        return event_list

    def get_event_target_path(self, session, event):
        tags = [profile.tag for profile in session.profiles]
        if 'target_path' in event.profile_tags.keys() and event.profile_tags['target_path'] in tags:
            provider = self.get_provider_from_profile_tag(event=event, tag='target_path')
            target_path = event.target_path
        else:
            provider = None
            target_path = None
        return provider, target_path

    def get_profile_from_profile_tag(self, session, event, tag):
        profile = None
        for p in session.profiles:
            if event.profile_tags[tag] == p.tag:
                return p
        return profile
