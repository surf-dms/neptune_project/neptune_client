import os
import configparser
import time

import cryptography
import typer
import tempfile
import json
from typing import Tuple
from cryptography.fernet import Fernet
from enum import Enum

from openapi_client import ProvenanceActionType
from rich.live import Live
from rich.progress import Progress
from rich.tree import Tree
from rich.panel import Panel
from rich import print


class OperationType(str, Enum):
    request = "request"
    upload = "upload"
    download = "download"
    share = "share"


def decrypt_secret(secret: str, key: str):
    cipher = Fernet(bytes.fromhex(key))
    try:
        return cipher.decrypt(bytes.fromhex(secret))
    except cryptography.fernet.InvalidToken:
        typer.echo("invalid encryption key")
        typer.Exit(1)


def encrypt_secret(secret: bytes, key: bytes = None):
    if not key:
        key = Fernet.generate_key()
    cipher = Fernet(key)
    return {"secret": cipher.encrypt(secret),
            "key": key}


def folder_size(path, total):
    for entry in os.scandir(path):
        if entry.is_file():
            total += entry.stat().st_size
        elif entry.is_dir():
            total = folder_size(entry.path, total)
    return total


def config_section_map(config, section):
    dict1 = {}
    options = config.options(section)
    for option in options:
        dict1[option] = config.get(section, option)
    return dict1


def parse_configuration(conf):
    conf_dict = {}
    if conf.is_file():
        config = configparser.ConfigParser()
        config.read(conf)
        conf_dict["default"] = config.defaults()
        for section in config.sections():
            conf_dict[section] = config_section_map(config, section)
    elif conf.is_dir():
        typer.echo("Config is a directory")
        raise typer.Abort()
    elif not conf.exists():
        typer.echo("The configuration file doesn't exist")
        raise typer.Abort()
    else:
        typer.echo("No config file")
        typer.echo("Try surfie --conf [FILE PATH]")
        raise typer.Abort()

    return conf_dict


def is_path_remote(path, neptune):
    provider_db = None
    tokens = path.split('::', 1)
    if tokens and len(tokens) > 1:
        provider_name = tokens[0]
        path = tokens[1]
        provider_db = neptune.get_provider_by_name(friendly_name=provider_name)
    return provider_db, path


def metadata_parser(meta, irods_connector=None):
    tokens = meta.split(',')
    metadata_list = []
    for token in tokens:
        metadata_list.append(extract_metadata(token, irods_connector=irods_connector))
    return metadata_list


# prefix scheme: 'f' for generic file, 't' for text, 'json' for json formatted file
def extract_metadata(meta, irods_connector=None):
    metadata = []
    tokens = meta.split('::', 1)
    if tokens and len(tokens) > 1:
        md_path = ''
        md_content = {}
        prefix = tokens[0]
        if prefix == "t":
            md_content = {"text": tokens[1]}
        elif prefix == "f":
            md_path = tokens[1]
        elif prefix == "json":
            md_path = tokens[1]
            with tempfile.TemporaryDirectory() as temp_dirname:
                if irods_connector:
                    size = irods_connector.getSize(itemPaths=[md_path])
                    irods_connector.downloadData(source=md_path, destination=temp_dirname, size=size, force=True)
                    obj_name = os.path.basename(md_path)
                    json_local_path = temp_dirname + os.path.sep + obj_name
                else:
                    json_local_path = md_path
                try:
                    with open(json_local_path) as json_file:
                        md_content = json.load(json_file)
                except ValueError as e:
                    typer.echo("invalid json {} as metadata input: {}".format(json_file, e))
                    typer.Exit(1)
        metadata = [prefix, md_path, md_content]
    else:
        metadata = ['', '', {"text": tokens[0]}]
    return metadata


def session_view_by_operation(operation, session):
    show = True
    show_approval = False
    request_target = False
    if operation:
        show = False
        for event in session.events:
            if event.operation == operation:
                show = True
                if event.operation == OperationType.request:
                    show_approval = True
                    if not event.target_path:
                        request_target = True
                break
    return show, show_approval, request_target


def session_detailed_view(session, session_username, neptune, veryverbose=False):
    session_tree = Tree("[bold]{} {} {} {} {}".format(session.code, session.status,
                                                      session.access, str(session.create_ts),
                                                      session_username))
    print(session_tree)
    with Progress(transient=True) as progress:
        displaying = progress.add_task("Processing...", total=len(session.events))
        for event in session.events:
            data_provider = None
            user_provider = None
            target_provider = None
            real_user = None
            user_attributes = {}
            for profile in session.profiles:
                if profile.tag == event.profile_tags['path']:
                    data_provider = neptune.get_provider_by_address(hostname=profile.endpoint.actual_instance.hostname,
                                                                    port=profile.endpoint.actual_instance.port)
                if 'user' in event.profile_tags.keys() and profile.tag == event.profile_tags['user']:
                    real_user = event.profile_tags['user'].split('::')[1]
                    user_provider = neptune.get_provider_by_address(hostname=profile.endpoint.actual_instance.hostname,
                                                                    port=profile.endpoint.actual_instance.port)
                    if 'user' in profile.to_dict()['metadata'].keys():
                        user_attributes = profile.metadata['user']
                if 'target_path' in event.profile_tags.keys() and profile.tag == event.profile_tags['target_path']:
                    target_provider = neptune.get_provider_by_address(hostname=profile.endpoint.actual_instance.hostname,
                                                                      port=profile.endpoint.actual_instance.port)

            data_provider_name = data_provider.friendly_name if data_provider else 'unknown'
            event_tree = session_tree.add("{} {} from provider {}".format(event.operation, event.path, data_provider_name))
            if real_user:
                user_provider_name = user_provider.friendly_name if user_provider else 'unknown'
                user_tree = event_tree.add("by user {} from provider {}".format(real_user, user_provider_name))
                if veryverbose:
                    for attr_key in user_attributes.keys():
                        user_tree.add("{}: {}".format(attr_key, user_attributes[attr_key]))
            if target_provider:
                target_provider_name = target_provider.friendly_name if target_provider else 'unknown'
                event_tree.add("to be copied to {} on provider {}".format(event.target_path, target_provider_name))
            print(event_tree)
            progress.update(displaying, advance=1)


def getSignatureAttributes(signature):
    signature_fields = signature.split('::')
    action = signature_fields[0]
    username = signature_fields[1]
    timestamp = signature_fields[2]
    related_session_code = None
    if len(signature_fields) == 4:
        related_session_code = signature_fields[3]
    return action, username, timestamp, related_session_code


def session_view_provenance(provenance, session_table, verbose=False, js=False):
    reviewer_list = []
    for signature in provenance:
        action, username, timestamp, related_session_code = getSignatureAttributes(signature)
        reviewer_list.append(username)
        if not js:
            msg = "signed by user {} on action {}, {}".format(username, action, timestamp)
            if related_session_code:
                msg = msg + ", related to session {}".format(related_session_code)
            if verbose:
                print(Panel.fit(msg))
            else:
                session_table.add_row(msg)
    return reviewer_list, session_table


def session_view_request_approval(session, neptune, request_approval, request_target):
    if request_approval:
        message = typer.style("Do you approve the request(s)?", fg=typer.colors.BRIGHT_BLUE)
        action = ProvenanceActionType.APPROVED if typer.confirm(message) else ProvenanceActionType.REJECTED
        #neptune.add_signature(session=session, action=action)
    else:
        message = typer.style("The request has been already reviewed", fg=typer.colors.BRIGHT_BLUE)
        typer.echo(message)
    if request_target and not (request_approval and action == ProvenanceActionType.REJECTED):
        message = typer.style("Do you want to specify a target location?", fg=typer.colors.BRIGHT_BLUE)
        if typer.confirm(message):
            target = typer.prompt("Please, enter the location (<provider name>::<path>)")
            provider_db, target_path = is_path_remote(target, neptune)
            # update session event and profile
            if provider_db:
                profile_tag, session = neptune.add_profile_from_provider(session=session,
                                                                         provider=provider_db,
                                                                         noaccess=True)
                # loop over events to add target_path and profile_tag
                events = []
                for event in session.events:
                    event.target_path = target_path
                    event.profile_tags["target_path"] = profile_tag
                    events.append(event)
                neptune.update_events(events=events, session_id=session.id)
            else:
                typer.echo("No provider found for location {}".format(target))
    if request_approval and action:
        neptune.add_signature(session=session, action=action)



# it checks if the owner of the session has already signed it for a specific action
def session_provenance_lookup(session, neptune, op, user_name=None):
    if session.provenance:
        if not user_name:
            session_user = neptune.get_user(session.data_user)
            user_name = session_user.username
        for signature in session.provenance:
            action, username, timestamp, related_session_code = getSignatureAttributes(signature)
            if action == op and username == user_name:
                return True
    return False


def profile_for_data_path_exists(session, event):
    tags = [profile.tag for profile in session.profiles]
    if event.profile_tags['path'] in tags or event.profile_tags['path'] == 'unknown':
        return True
    return False


def preserve_source_tree(source_path, target_path, levels_to_skip=0):
    tokens = source_path.split(os.path.sep, levels_to_skip + 1)
    source_relative_tree = tokens[levels_to_skip + 1] if len(tokens) > levels_to_skip + 1 else ''
    return os.path.normpath(target_path + os.path.sep + source_relative_tree)
