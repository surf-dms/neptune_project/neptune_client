import hashlib
import logging
import os

BUF_SIZE = 1024 * 1024 * 4


class fsConnector():
    log = logging.getLogger(__name__)

    def __init__(self):
        self.log.debug("initialize fs connector")

    def calculate_file_checksum(self, file_path, chk_type='sha256'):
        if chk_type == 'sha256':
            sha256 = hashlib.sha256()
            with open(file_path, 'rb') as f:
                while True:
                    data = f.read(BUF_SIZE)
                    if not data:
                        break
                    sha256.update(data)
            return sha256.hexdigest()
        else:
            raise Exception("Unsupported checksum algorithm")

    def isFile(self, path):
        return os.path.isfile(path)

    def pathExists(self, path):
        return os.path.exists(path)

    def getChecksum(self, path, chk_type='sha256'):
        chk_dict = {}
        if self.pathExists(path):
            if self.isFile(path):
                chk_sum = self.calculate_file_checksum(path, chk_type)
                chk_dict = {path: {'type': chk_type, 'sum': chk_sum}}
            else:
                for rootpath, folders, files in os.walk(path):
                    for filename in files:
                        chk_path = os.path.join(rootpath, filename)
                        chk_sum = self.calculate_file_checksum(chk_path, chk_type)
                        chk_dict[chk_path] = {'type': chk_type, 'sum': chk_sum}
            return chk_dict
        else:
            raise Exception("Path {} does not exist".format(path))

    def getSize(self, path):
        size = 0
        if self.pathExists(path):
            if self.isFile(path):
                size = os.stat(path).st_size
            else:
                for rootpath, folders, files in os.walk(path):
                    for filename in files:
                        full_path = os.path.join(rootpath, filename)
                        size = size + os.stat(full_path).st_size
            return size
        else:
            raise Exception("Path {} does not exist".format(path))
