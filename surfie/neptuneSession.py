import logging
import typer
import os

from irods.access import iRODSAccess
from openapi_client.models import SessionBase

from surfie import helpers
from surfie.irodswave.intorods import intorods

from surfie.neptunewave import neptune
from surfie.irodswave import irodsConnector
from surfie.fswave import fsConnector


def initialize_irods_session(config_dict, provider_friendly_name):
    if not (provider_friendly_name in config_dict.keys()):
        config_dict[provider_friendly_name] = {}
    return irodsConnector.irodsConnector(config_dict[provider_friendly_name])


def close_irods_session(irods_connector):
    irods_connector.close()


class neptuneSession():
    log = logging.getLogger(__name__)

    def __init__(self, neptune, config_dict, code, session, session_type='private', shared_list=None, lifetime=None):
        if shared_list is None:
            shared_list = []
        self.neptune = neptune
        self.config_dict = config_dict
        self.code = code
        self.encryption_key = None
        self.events_dict = {}
        self.lifetime = lifetime if lifetime else 3600
        self.access = session_type

        if session:
            self.session = session
            self.participants = session.participants
            if session.access == "private":
                self.encryption_key = typer.prompt("You want to share other data in a private session, "
                                                   "please specify the encryption key")
            else:
                self.log.debug("Get session of type {}".format(session.access))
                self.access = session.access
        else:
            participants = []
            if session_type == 'public':
                self.log.debug("creating a public session")
            elif session_type == 'shared' and shared_list:
                self.log.debug("creating a session shared with {}".format(shared_list))
                participants = neptune.get_data_users_id(shared_list)
            self.participants = participants
            self.session = SessionBase(lifetime=self.lifetime, access=self.access, participants=participants)


    def finalize(self):
        self.session = self.neptune.finalize_session(session_base=self.session)

    def set_log(self, log):
        self.log = log

    def analyze_data_path(self, provider, data_path):
        event_dict = {}
        event_dict['normalized_path'] = data_path
        event_dict['provider'] = provider
        if provider:
            event_dict['remote_source'] = True
            event_dict['endpoint_type'] = []
            # looking for the provider endpoint in Neptune
            for endpoint in provider.endpoints:
                event_dict['endpoint_type'].append(endpoint.actual_instance.ep_type)
            if not ('endpoint_type' in event_dict.keys()):
                typer.echo("No known endpoint available for provider {}".format(provider.friendly_name))
                typer.Exit(0)
            self.events_dict[provider.friendly_name + '::' + data_path] = event_dict
        else:
            # looking for the shared iRODS space in Neptune
            event_dict['remote_source'] = False
            event_dict['endpoint_type'] = ['irods']
            self.events_dict[data_path] = event_dict

    def is_data_path_remote(self, path):
        provider_db, data_path = helpers.is_path_remote(path, self.neptune)
        self.analyze_data_path(provider=provider_db, data_path=data_path)
        if provider_db:
            return True
        else:
            return False

    def add_metadata(self, meta, path):

        irods_connector = None
        if (path in self.events_dict.keys() and 'remote_source' in self.events_dict[path].keys()
            and self.events_dict[path]['remote_source']):
            data_provider = self.events_dict[path]['provider']
            endpoint_type = data_provider.endpoint_default_type
            if data_provider.friendly_name in self.config_dict.keys():
                if "endpoint_type" in self.config_dict[data_provider.friendly_name].keys():
                    endpoint_type = self.config_dict[data_provider.friendly_name]["endpoint_type"]
            if 'endpoint_type' in self.events_dict[path].keys():
                if 'irods' in self.events_dict[path]['endpoint_type'] and endpoint_type == 'irods':
                    irods_connector = initialize_irods_session(self.config_dict, data_provider.friendly_name)

        metadata_list = helpers.metadata_parser(meta, irods_connector)
        self.log.debug("adding metadata {}".format(metadata_list))
        if path in self.events_dict.keys():
            if 'metadata' in self.events_dict[path].keys():
                self.events_dict[path]['metadata'] += metadata_list
            else:
                self.events_dict[path]['metadata'] = metadata_list
        else:
            self.events_dict[path] = {'metadata': metadata_list}
        if irods_connector:
            irods_connector.close()

    def add_system_metadata(self, path):
        irods_connector = None
        metadata_list = []
        if path in self.events_dict.keys():
            if ('remote_source' in self.events_dict[path].keys() and self.events_dict[path]['remote_source']):
                data_provider = self.events_dict[path]['provider']

                endpoint_type = data_provider.endpoint_default_type
                if data_provider.friendly_name in self.config_dict.keys():
                    if "endpoint_type" in self.config_dict[data_provider.friendly_name].keys():
                        endpoint_type = self.config_dict[data_provider.friendly_name]["endpoint_type"]

                if 'endpoint_type' in self.events_dict[path].keys():
                    if 'irods' in self.events_dict[path]['endpoint_type'] and endpoint_type == 'irods':
                        irods_connector = initialize_irods_session(self.config_dict, data_provider.friendly_name)
                        restore_acl = None
                        try:
                            npath = self.events_dict[path]['normalized_path']
                            if not irods_connector.isAccessible(target_path=npath, access_type='write'):
                                irods_connector.getAccess(target_path=npath, access_type='write')
                                restore_acl = npath
                        except Exception as e:
                            prov_name = data_provider.friendly_name
                            typer.echo("No access to path {}, on provider {}, for user {}".format(npath,
                                                                                                      prov_name,
                                                                                                      self.config_dict[prov_name]['user']))
                            raise e
                        path_size = irods_connector.getSize([npath])
                        path_chksum = irods_connector.getChecksum(path=npath, compute=True)
                        if irods_connector.session.data_objects.exists(npath):
                            path_type = 'dataobject'
                        else:
                            path_type = 'collection'
                        metadata = ['system', path, {'type': path_type, 'size': path_size, 'checksum': path_chksum}]
                        metadata_list.append(metadata)
                    elif 'davrods' in self.events_dict[path]['endpoint_type'] and endpoint_type == 'davrods':
                        pass
            else:
                # compute local system metadata
                fs_conn = fsConnector.fsConnector()
                path_size = fs_conn.getSize(path)
                path_chksum = fs_conn.getChecksum(path)
                if fs_conn.isFile(path):
                    path_type = 'dataobject'
                else:
                    path_type = 'collection'
                metadata = ['system', path, {'type': path_type, 'size': path_size, 'checksum': path_chksum}]
                metadata_list.append(metadata)

        self.log.debug("adding metadata {}".format(metadata_list))
        if path in self.events_dict.keys():
            if 'metadata' in self.events_dict[path].keys():
                self.events_dict[path]['metadata'] += metadata_list
            else:
                self.events_dict[path]['metadata'] = metadata_list
        else:
            self.events_dict[path] = {'metadata': metadata_list}
        if irods_connector:
            if restore_acl:
                username = irods_connector.session.username
                zone = irods_connector.session.zone
                irods_connector.setPermissions(rights='null', user=username, path=restore_acl, zone=zone,
                                               recursive=True)
            irods_connector.close()

    def add_system_metadata(self, path):
        irods_connector = None
        metadata_list = []
        if path in self.events_dict.keys():
            if ('remote_source' in self.events_dict[path].keys() and self.events_dict[path]['remote_source']):
                data_provider = self.events_dict[path]['provider']

                endpoint_type = data_provider.endpoint_default_type
                if data_provider.friendly_name in self.config_dict.keys():
                    if "endpoint_type" in self.config_dict[data_provider.friendly_name].keys():
                        endpoint_type = self.config_dict[data_provider.friendly_name]["endpoint_type"]

                if 'endpoint_type' in self.events_dict[path].keys():
                    if 'irods' in self.events_dict[path]['endpoint_type'] and endpoint_type == 'irods':
                        irods_connector = initialize_irods_session(self.config_dict, data_provider.friendly_name)
                        restore_acl = None
                        try:
                            npath = self.events_dict[path]['normalized_path']
                            if not irods_connector.isAccessible(target_path=npath, access_type='write'):
                                irods_connector.getAccess(target_path=npath, access_type='write')
                                restore_acl = npath
                        except Exception as e:
                            prov_name = data_provider.friendly_name
                            typer.echo("No access to path {}, on provider {}, for user {}".format(npath,
                                                                                            prov_name,
                                                                                            self.config_dict[prov_name]['user']))
                            raise e
                        path_size = irods_connector.getSize([npath])
                        path_chksum = irods_connector.getChecksum(path=npath, compute=True)
                        if irods_connector.session.data_objects.exists(npath):
                            path_type = 'dataobject'
                        else:
                            path_type = 'collection'
                        metadata = ['system', path, {'type': path_type, 'size': path_size, 'checksum': path_chksum}]
                        metadata_list.append(metadata)
                    elif 'davrods' in self.events_dict[path]['endpoint_type'] and endpoint_type == 'davrods':
                        pass
            else:
                # compute local system metadata
                fs_conn = fsConnector.fsConnector()
                path_size = fs_conn.getSize(path)
                path_chksum = fs_conn.getChecksum(path)
                if fs_conn.isFile(path):
                    path_type = 'dataobject'
                else:
                    path_type = 'collection'
                metadata = ['system', path, {'type': path_type, 'size': path_size, 'checksum': path_chksum}]
                metadata_list.append(metadata)

        self.log.debug("adding metadata {}".format(metadata_list))
        if path in self.events_dict.keys():
            if 'metadata' in self.events_dict[path].keys():
                self.events_dict[path]['metadata'] += metadata_list
            else:
                self.events_dict[path]['metadata'] = metadata_list
        else:
            self.events_dict[path] = {'metadata': metadata_list}
        if irods_connector:
            if restore_acl:
                username = irods_connector.session.username
                zone = irods_connector.session.zone
                irods_connector.setPermissions(rights='null', user=username, path=restore_acl, zone=zone,
                                               recursive=True)
            irods_connector.close()

    def get_profiles(self, paths, secret=None, noaccess=None, profile_tags=None):
        endpoint_type = None
        profile_tags = profile_tags if profile_tags else {}
        for key in paths.keys():
            if self.events_dict[paths[key]]['remote_source']:
                data_provider = self.events_dict[paths[key]]['provider']
                if data_provider.friendly_name in self.config_dict.keys():
                    if "endpoint_type" in self.config_dict[data_provider.friendly_name].keys():
                        endpoint_type = self.config_dict[data_provider.friendly_name]["endpoint_type"]
                profile_tag, self.session = self.neptune.add_profile_from_provider(session=self.session,
                                                                                   provider=data_provider,
                                                                                   secret=secret, noaccess=noaccess,
                                                                                   endpoint_type=endpoint_type)
                if profile_tag:
                    profile_tags[key] = profile_tag
        return profile_tags

    def add_session_event(self, op, path, target_path, noaccess=None, user=None):

        ticket_string = None
        secret = None
        profile_tags = {'path': 'unknown'}
        paths_dict = {'path': path}
        if target_path:
            paths_dict['target_path'] = target_path
            target_data_path = self.events_dict[target_path]['normalized_path']
        else:
            target_data_path = None
        data_path = self.events_dict[path]['normalized_path']

        if op == 'request':
            self.log.debug("creating a request event for {}".format(path))
            if user:
                if 'metadata' in self.events_dict[user]:
                    metadata = self.events_dict[user]['metadata'][0]
                    user_md = {'user': metadata[2]}
                else:
                    user_md = None
                endpoint_type = None
                user_provider, user_path = helpers.is_path_remote(user, self.neptune)
                if user_provider.friendly_name in self.config_dict.keys():
                    if "endpoint_type" in self.config_dict[user_provider.friendly_name].keys():
                        endpoint_type = self.config_dict[user_provider.friendly_name]["endpoint_type"]
                profile_tags['user'], self.session = self.neptune.add_profile_from_provider(session=self.session,
                                                                                            provider=user_provider,
                                                                                            noaccess=noaccess,
                                                                                            secret=secret,
                                                                                            endpoint_type=endpoint_type,
                                                                                            username=user_path,
                                                                                            metadata=user_md)
        else:
            if 'irods' in self.events_dict[path]['endpoint_type']:
                if not noaccess:
                    if self.session.access in ["public", "shared"]:
                        self.log.debug("adding a ticket for target path {}".format(target_path))
                        if op == "upload":
                            data_provider = self.events_dict[target_path]['provider']
                            ticket_path = target_data_path + '/' + os.path.basename(data_path)
                        else:
                            data_provider = self.events_dict[path]['provider']
                            ticket_path = data_path
                        irods_connector = initialize_irods_session(self.config_dict, data_provider.friendly_name)
                        if self.lifetime:
                            self.log.debug("ticket duration in seconds: {}".format(str(self.lifetime)))
                        ticket_string, exp_flag = irods_connector.createTicket(path=ticket_path, expiryInterval=self.lifetime)
                        self.log.debug("ticket string: {}".format(ticket_string))
                        irods_connector.close()
                    else:
                        self.log.debug("adding an encrypted secret to the private session")
                        if op == "upload":
                            data_provider = self.events_dict[target_path]['provider']
                        else:
                            data_provider = self.events_dict[path]['provider']
                        encrypted_context = helpers.encrypt_secret(
                            str.encode(self.config_dict[data_provider.friendly_name]["password"]),
                            self.encryption_key)
                        secret = encrypted_context["secret"].hex()
                        typer.echo("encryption key: {}".format(encrypted_context["key"].hex()))

        profile_tags = self.get_profiles(paths=paths_dict, secret=secret, noaccess=noaccess, profile_tags=profile_tags)

        if 'metadata' in self.events_dict[path]:
            metadata = self.events_dict[path]['metadata']
        else:
            metadata = None

        self.session = self.neptune.add_event(session=self.session, operation=op, path=data_path,
                                              target_path=target_data_path,
                                              profile_tags=profile_tags, metadata=metadata, ticket=ticket_string)

    def import_events(self, events_list, operation=None):
        for triple in events_list:
            event = triple[0]
            op = operation if operation else event.operation
            path = triple[1] + '::' + event.path
            self.is_data_path_remote(path=path)
            try:
                self.add_system_metadata(path)
            except Exception as e:
                self.log.error("Cannot add system metadata: {}".format(type(e)))
                continue
            target_path = None
            if triple[2]:
                target_path = triple[2] + '::' + event.target_path
                self.is_data_path_remote(path=target_path)
            self.add_session_event(op=op, path=path, target_path=target_path)

    def configure_credentials(self, profile, event, encryption_key=None):
        ticket_string = None
        credential_dict = {}
        if profile.secret_type != "ticket":
            if not encryption_key:
                exc_msg = "Missing encryption key"
                typer.echo(exc_msg)
                typer.echo("Try surfie session --encryption [ENCRYPTION KEY]")
                raise Exception(exc_msg)
            else:
                self.log.debug("secret is not a ticket and encryption key found")
                decrypted_secret = helpers.decrypt_secret(profile.secret, encryption_key)
                credential_dict["password"] = decrypted_secret.decode()
        else:
            self.log.debug("secret is a ticket")
            ticket_string = event.ticket
            credential_dict["password"] = ''

        credential_dict["user"] = profile.username.strip()
        credential_dict["host"] = profile.endpoint.actual_instance.hostname.strip()
        credential_dict["port"] = profile.endpoint.actual_instance.port
        credential_dict["zone"] = profile.endpoint.actual_instance.zone.strip()
        credential_dict["auth_scheme"] = profile.auth_scheme.strip()
        return ticket_string, credential_dict

    def download_data_by_operations(self, operations, target=None, encryption_key=None, preserve_source_tree=False):

        if target:
            target_provider, target_path = helpers.is_path_remote(target, self.neptune)
        else:
            target_provider = None
            target_path = None

        downloaded = False
        for event in self.session.events:
            if event.operation in operations:
                # check that the access details are available
                if not helpers.profile_for_data_path_exists(self.session, event):
                    typer.echo("Missing connection details in the session for path {}".format(event.path))
                    self.log.debug("Session: {} - Profile tag {} not found for path {}".format(self.session.code,
                                                                                               event.profile_tags['path'],
                                                                                               event.path))
                    continue
                # define the source and the destination
                if event.operation == "upload":
                    filename = os.path.basename(event.path)
                    source_path = event.target_path + '/' + filename
                    profile = self.neptune.get_profile_from_profile_tag(session=self.session, event=event, tag="target_path")
                    target_provider = target_provider if target else None
                else:
                    source_path = event.path
                    profile = self.neptune.get_profile_from_profile_tag(session=self.session, event=event, tag="path")
                    event_target_provider, event_target_path = self.neptune.get_event_target_path(self.session, event)
                    target_provider = event_target_provider if not target else target_provider
                    target_path = event_target_path if not target_path else target_path
                target_path = target_path if target_path else '.'
                self.analyze_data_path(provider=target_provider, data_path=target_path)

                # get the ticket from the event profile and set the config dictionary
                try:
                    ticket_string, credentials = self.configure_credentials(profile=profile, event=event,
                                                                            encryption_key=encryption_key)
                except Exception as error:
                    self.log.error("Error in getting credentials: {}".format(error))
                    continue
                # if there is a remote provider then use intorods, otherwise irodsConnector
                if target_provider:
                    prov_name = target_provider.friendly_name
                    endpoint_type = profile.endpoint.actual_instance.ep_type
                    for md in event.metadata:
                        if md[0] == 'system':
                            source_type = md[2]['type']
                            break
                    if (endpoint_type in self.events_dict[prov_name + '::' + target_path]['endpoint_type']
                            and endpoint_type == 'irods'):
                        # check if the target path is accessible
                        try:
                            irods_connector = irodsConnector.irodsConnector(self.config_dict[target_provider.friendly_name])
                            if not irods_connector.isAccessible(target_path=target_path, access_type='write'):
                                irods_connector.getAccess(target_path=target_path, access_type='write')
                                restore_acl = target_path
                            else:
                                restore_acl = None
                                irods_connector.close()
                        except Exception as e:
                            typer.echo("No access to path {}, on provider {}, for user {}".format(target_path, prov_name,
                                                                                                  self.config_dict[
                                                                                                      prov_name]['user']))
                            self.log.error("Error in getting permissions: {}".format(type(e)))
                            continue

                        # preserve the source tree structure, skipping /zone/home/root_folder
                        if preserve_source_tree:
                            source_coll = source_path
                            if source_type == 'dataobject':
                                source_coll = os.path.dirname(source_path)
                            target_path = helpers.preserve_source_tree(source_path=source_coll, target_path=target_path,
                                                                       levels_to_skip=3)

                        metadata = {'{}::data_complete'.format(prov_name): '{}'.format(source_path)}
                        success = intorods.replicate_data_wrapper(source_fs='irods',
                                                                  source_connect_dict=credentials,
                                                                  source_path=source_path,
                                                                  dest_connection_dict=self.config_dict[prov_name],
                                                                  target_path=target_path,
                                                                  metadata=metadata,
                                                                  ticket_string=ticket_string,
                                                                  compareChecksum=True
                                                                  )
                        if restore_acl:
                            username = irods_connector.session.username
                            zone = irods_connector.session.zone
                            irods_connector.setPermissions(rights='null', user=username, path=restore_acl, zone=zone, recursive=True)
                            irods_connector.close()
                        downloaded = success
                else:
                    irods_connector = irodsConnector.irodsConnector(credentials)
                    size = irods_connector.getSize([source_path], ticket_string=ticket_string)
                    irods_connector.downloadData(source=source_path, destination=target_path, size=size,
                                                 ticket_string=ticket_string)
                    irods_connector.close()
                    downloaded = True
        return downloaded
