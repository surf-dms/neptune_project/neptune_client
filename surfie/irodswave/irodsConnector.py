import getpass
import hashlib
import logging
import os
import random
import string
import datetime
from base64 import b64decode
from pathlib import Path
from shutil import disk_usage

import irods.keywords as kw
from irods.access import iRODSAccess
from irods.column import Like, Criterion
from irods.exception import CATALOG_ALREADY_HAS_ITEM_BY_THAT_NAME, \
     CAT_NO_ACCESS_PERMISSION, CAT_SUCCESS_BUT_WITH_NO_INFO, \
     CAT_INVALID_ARGUMENT, CAT_INVALID_USER, CAT_INVALID_AUTHENTICATION, \
     NetworkException, PAM_AUTH_PASSWORD_FAILED, USER_INVALID_USERNAME_FORMAT
from irods.models import Collection, DataObject, Resource, CollectionMeta, DataObjectMeta
from irods.models import User, UserGroup
from irods.path import iRODSPath
from irods.rule import Rule
from irods.session import iRODSSession
from irods.ticket import Ticket

RED = '\x1b[1;31m'
DEFAULT = '\x1b[0m'
YEL = '\x1b[1;33m'
BLUE = '\x1b[1;34m'


class irodsConnector():
    log = logging.getLogger(__name__)

    def __init__(self, config_dict):

        try:
            env_file = os.environ['IRODS_ENVIRONMENT_FILE']
            iRODS_session = iRODSSession(irods_env_file=env_file)
        except KeyError:
            if os.path.exists('~/.irods/irods_environment.json'):
                env_file = os.path.expanduser('~/.irods/irods_environment.json')
                iRODS_session = iRODSSession(irods_env_file=env_file)
            else:
                if not ('user' in config_dict.keys() and config_dict["user"]):
                    config_dict["user"] = input("Please enter iRODS user: ")
                if config_dict["user"] != 'anonymous':
                    if not ('password' in config_dict.keys() and config_dict["password"]):
                        pw = getpass.getpass().encode()
                        passwd = pw.decode()
                        config_dict["password"] = passwd
                else:
                    config_dict["password"] = ''
                if not ('host' in config_dict.keys() and config_dict["host"]):
                    config_dict["host"] = input("Please enter iRODS host: ")
                if not ('port' in config_dict.keys() and config_dict["port"]):
                    config_dict["port"] = input("Please enter iRODS port: ")
                if not ('zone' in config_dict.keys() and config_dict["zone"]):
                    config_dict["zone"] = input("Please enter iRODS zone: ")
                if not ('auth_scheme' in config_dict.keys() and config_dict["auth_scheme"]):
                    config_dict["auth_scheme"] = input("Please enter authentication scheme [native or pam]: ")
                ssl_settings = {'client_server_negotiation': 'request_server_negotiation',
                                'client_server_policy': 'CS_NEG_REQUIRE',
                                'encryption_algorithm': 'AES-256-CBC',
                                'encryption_key_size': 32,
                                'encryption_num_hash_rounds': 16,
                                'encryption_salt_size': 8
                                }
                self.log.debug("iRODS session: {}".format(config_dict))
                iRODS_session = iRODSSession(host=config_dict["host"], port=config_dict["port"],
                                             user=config_dict["user"], password=config_dict["password"],
                                             zone=config_dict["zone"], authentication_scheme=config_dict["auth_scheme"],
                                             **ssl_settings)
        except Exception as e:
            self.log.error("Wrong iRODS session: {}".format(e))
            raise

        with iRODS_session as session:

            try:
                self.log.debug("server version: {}".format(session.server_version))
                user = session.users.get(session.username)
                self.log.debug("session user: {}, {}".format(session.username, user.id))
                self.session = session
                # mapping to manage 4.2 and 4.3 different ACL names
                VERSION_DEPENDENT_STRINGS = {'MODIFY': 'modify_object',
                                             'READ': 'read_object'} if self.session.server_version >= (4, 3) \
                                             else {'MODIFY': 'modify object', 'READ': 'read object'}
                self.acl_mapping = {
                    'write': VERSION_DEPENDENT_STRINGS['MODIFY'],
                    'read': VERSION_DEPENDENT_STRINGS['READ'],
                    'own': 'own'
                }
            except (USER_INVALID_USERNAME_FORMAT, CAT_INVALID_USER) as e:
                self.log.error("Wrong username: {}".format(e))
                raise
            except (PAM_AUTH_PASSWORD_FAILED, CAT_INVALID_AUTHENTICATION) as e:
                self.log.error("Wrong password: {}".format(e))
                raise
            except NetworkException as e:
                self.log.error("Wrong network parameters: {}".format(e))
                raise
            except Exception as e:
                self.log.error("Error in connecting to iRODS: {}".format(e))
                raise

    def getUserInfo(self):
        userGroupQuery = self.session.query(UserGroup).filter(Like(User.name, self.session.username))
        userTypeQuery = self.session.query(User.type).filter(Like(User.name, self.session.username))

        userType = []
        for t in userTypeQuery.get_results():
            userType.extend(list(t.values()))
        userGroups = []
        for g in userGroupQuery.get_results():
            userGroups.extend(list(g.values()))

        return (userType, userGroups)

    def getPermissions(self, iPath):
        '''
        iPath: Can be a string or an iRODS collection/object
        Throws:
            irods.exception.CollectionDoesNotExist
        '''
        try:
            return self.session.acls.get(iPath)
        except:
            try:
                coll = self.session.collections.get(iPath)
                return self.session.acls.get(coll)
            except:
                try:
                    obj = self.session.data_objects.get(iPath)
                    return self.session.acls.get(obj)
                except:
                    logging.info('GET PERMISSIONS', exc_info=True)
                    raise

    def setPermissions(self, rights, user, path, zone, recursive=False):
        '''
        Sets permissions to an iRODS collection or data object.
        path: string
        rights: string, [own, read, write, null]
        '''
        acl = iRODSAccess(rights, path, user, zone)
        userTypes, groups = self.getUserInfo()
        adminFlag = True if userTypes.pop() == 'rodsadmin' else False
        try:
            if recursive and self.session.collections.exists(path):
                self.session.acls.set(acl, recursive=True, admin=adminFlag)
            else:
                self.session.acls.set(acl, recursive=False, admin=adminFlag)
        except CAT_INVALID_USER:
            raise CAT_INVALID_USER("ACL ERROR: user unknown ")
        except CAT_INVALID_ARGUMENT:
            print(RED + "ACL ERROR: rights or path not known " + path + DEFAULT)
            logging.info('ACL ERROR: rights or path not known', exc_info=True)
            raise

    def ensureColl(self, collPath):
        '''
        Raises:
            irods.exception.CAT_NO_ACCESS_PERMISSION
        '''
        try:
            self.session.collections.create(collPath)
            return self.session.collections.get(collPath)
        except:
            logging.info('ENSURE COLLECTION', exc_info=True)
            raise

    def search(self, keyVals=None):
        '''
        Given a dictionary with keys and values, searches for collections and
        data objects that fullfill the criteria.
        The key 'checksum' will be mapped to DataObject.checksum, the key 'path'
        will be mapped to Collection.name and the key 'object' will be mapped to DataObject.name.
        Default: if no keyVals are given, all accessible collections and data objects will be returned

        keyVals: dict; {'checksum': '', 'key1': 'val1', 'key2': 'val2', 'path': '', 'object': ''}

        Returns:
        list: [[Collection name, Object name, checksum]]
        '''
        collQuery = None
        objQuery = None
        # data query
        if 'checksum' in keyVals or 'object' in keyVals:
            objQuery = self.session.query(Collection.name, DataObject.name, DataObject.checksum)
            if 'object' in keyVals:
                if keyVals['object']:
                    objQuery = objQuery.filter(Like(DataObject.name, keyVals['object']))
            if 'checksum' in keyVals:
                if keyVals['checksum']:
                    objQuery = objQuery.filter(Like(DataObject.checksum, keyVals['checksum']))
        else:
            collQuery = self.session.query(Collection.name)
            objQuery = self.session.query(Collection.name, DataObject.name, DataObject.checksum)

        if 'path' in keyVals and keyVals['path']:
            if collQuery:
                collQuery = collQuery.filter(Like(Collection.name, keyVals['path']))
            objQuery = objQuery.filter(Like(Collection.name, keyVals['path']))

        for key in keyVals:
            if key not in ['checksum', 'path', 'object']:
                if objQuery:
                    objQuery.filter(DataObjectMeta.name == key)
                if collQuery:
                    collQuery.filter(CollectionMeta.name == key)
                if keyVals[key]:
                    if objQuery:
                        objQuery.filter(DataObjectMeta.value == keyVals[key])
                    if collQuery:
                        collQuery.filter(CollectionMeta.value == keyVals[key])

        results = [['', '', ''], ['', '', ''], ['', '', '']]
        collBatch = [[]]
        objBatch = [[]]
        # return only 100 results
        if collQuery:
            results[0] = ["Collections found: " + str(sum(1 for _ in collQuery)), '', '']
            collBatch = [b for b in collQuery.get_batches()]
        if objQuery:
            results[1] = ["Objects found: " + str(sum(1 for _ in objQuery)), '', '']
            objBatch = [b for b in objQuery.get_batches()]

        for res in collBatch[0][:50]:
            results.append([res[list(res.keys())[0]], '', ''])
        for res in objBatch[0][:50]:
            results.append([res[list(res.keys())[0]],
                            res[list(res.keys())[1]],
                            res[list(res.keys())[2]]])
        return results

    def listResources(self):
        """
        Returns list of all root resources, that accept data.
        """
        query = self.session.query(Resource.name, Resource.parent)
        resources = []
        for item in query.get_results():
            rescName, parent = item.values()
            if parent == None:
                resources.append(rescName)

        if 'bundleResc' in resources:
            resources.remove('bundleResc')
        if 'demoResc' in resources:
            resources.remove('demoResc')

        return resources

    def getResource(self, resource):
        '''
        Raises:
            irods.exception.ResourceDoesNotExist
        '''
        return self.session.resources.get(resource)

    def resourceSize(self, resource):
        """
        Returns the available space left on a resource in bytes
        resource: Name of the resource
        Throws: ResourceDoesNotExist if resource not known
                AttributeError if 'free_space' not set
        """
        try:
            size = self.session.resources.get(resource).free_space
            return size
        except Exception as error:
            logging.info('RESOURCE ERROR: Either resource does not exist or size not set.',
                         exc_info=True)
            raise error("RESOURCE ERROR: Either resource does not exist or size not set.")

    def uploadData(self, source, destination, resource, size, buff=1024 ** 3,
                   force=False, diffs=[]):
        """
        source: absolute path to file or folder
        destination: iRODS collection where data is uploaded to
        resource: name of the iRODS storage resource to use
        size: size of data to be uploaded in bytes
        buff: buffer on resource that should be left over
        force: If true, do not calculate storage capacity on destination
        diffs: output of diff functions

        The function uploads the contents of a folder with all subfolders to 
        an iRODS collection.
        If source is the path to a file, the file will be uploaded.

        Throws:
        ResourceDoesNotExist
        ValueError (if resource too small or buffer is too small)
        
        """
        logging.info('iRODS UPLOAD: ' + str(source) + '-->' + str(destination) + ', ' + str(resource))
        if resource is not None and resource != "":
            options = {kw.RESC_NAME_KW: resource,
                       kw.REG_CHKSUM_KW: '', kw.ALL_KW: '',
                       kw.VERIFY_CHKSUM_KW: ''}
        else:
            options = {kw.REG_CHKSUM_KW: '', kw.ALL_KW: '',
                       kw.VERIFY_CHKSUM_KW: ''}

        if diffs == []:
            if os.path.isfile(source):
                (diff, onlyFS, onlyIrods, same) = self.diffObjFile(
                    destination + '/' + os.path.basename(source),
                    source, scope="checksum")
            elif os.path.isdir(source):
                subcoll = self.session.collections.create(
                    destination + '/' + os.path.basename(source))
                (diff, onlyFS, onlyIrods, same) = self.diffIrodsLocalfs(subcoll, source)
            else:
                raise FileNotFoundError("ERROR iRODS upload: not a valid source path")
        else:
            (diff, onlyFS, onlyIrods, same) = diffs

        if not force:
            try:
                space = self.session.resources.get(resource).free_space
                if not space:
                    logging.info(
                        'ERROR iRODS upload: No size set on iRODS resource. Refuse to upload.',
                        exc_info=True)
                    raise ValueError(
                        'ERROR iRODS upload: No size set on iRODS resource. Refuse to upload.')
                if int(size) > (int(space) - buff):
                    logging.info('ERROR iRODS upload: Not enough space on resource.',
                                 exc_info=True)
                    raise ValueError('ERROR iRODS upload: Not enough space on resource.')
                if buff < 0:
                    logging.info('ERROR iRODS upload: Negative resource buffer.',
                                 exc_info=True)
                    raise BufferError('ERROR iRODS upload: Negative resource buffer.')
            except Exception as error:
                logging.info('UPLOAD ERROR', exc_info=True)
                raise error

        if os.path.isfile(source) and len(diff + onlyFS) > 0:
            try:
                #print("IRODS UPLOADING FILE", destination + "/" + os.path.basename(source))
                logging.debug("Creating the collection {}".format(destination))
                self.session.collections.create(destination)
                self.session.data_objects.put(
                    source, destination + "/" + os.path.basename(source),
                    num_threads=4, **options)
                return
            except IsADirectoryError:
                logging.info('IRODS UPLOAD ERROR: There exists a collection of same name as ' + source,
                             exc_info=True)
                print("IRODS UPLOAD ERROR: There exists a collection of same name as " + source)
                raise

        try:  # collections/folders
            logging.info("IRODS UPLOAD started:")
            for d in diff:
                # upload files to distinct data objects
                destColl = self.session.collections.create(os.path.dirname(d[0]))
                logging.info("REPLACE: " + d[0] + " with " + d[1])
                self.session.data_objects.put(d[1], d[0], num_threads=4, **options)

            for o in onlyFS:  # can contain files and folders
                # Create subcollections and upload
                sourcePath = os.path.join(source, o)
                if len(o.split(os.sep)) > 1:
                    subColl = self.session.collections.create(
                        destination + '/' + os.path.basename(source) + '/' + os.path.dirname(o))
                else:
                    subColl = self.session.collections.create(
                        destination + '/' + os.path.basename(source))
                logging.info("UPLOAD: " + sourcePath + " to " + subColl.path)
                logging.info("CREATE " + subColl.path + "/" + os.path.basename(sourcePath))
                self.session.data_objects.put(
                    sourcePath, subColl.path + "/" + os.path.basename(sourcePath),
                    num_threads=0, **options)
        except Exception as error:
            logging.info('UPLOAD ERROR', exc_info=True)
            raise

    def downloadData(self, source, destination, size, buff=1024 ** 3, force=False, diffs=[], ticket_string=None):
        '''
        Download object or collection.
        source: iRODS collection or data object
        destination: absolute path to download folder
        size: size of data to be downloaded in bytes
        buff: buffer on resource that should be left over
        force: If true, do not calculate storage capacity on destination
        diffs: output of diff functions
        '''
        logging.info('iRODS DOWNLOAD: ' + str(source) + '-->' + str(destination))
        options = {kw.FORCE_FLAG_KW: '', kw.REG_CHKSUM_KW: ''}

        if destination.endswith(os.sep):
            destination = destination[:len(destination) - 1]
        if not os.path.isdir(destination):
            logging.info('DOWNLOAD ERROR: destination path does not exist or is not directory', exc_info=True)
            raise FileNotFoundError(
                "ERROR iRODS download: destination path does not exist or is not directory")
        if not os.access(destination, os.W_OK):
            logging.info('DOWNLOAD ERROR: No rights to write to destination.', exc_info=True)
            raise PermissionError("ERROR iRODS download: No rights to write to destination.")

        if ticket_string:
            logging.debug("Using the ticket {}".format(ticket_string))
            Ticket(self.session, ticket_string).supply()

        if diffs == []:  # Only download if not present or difference in files
            if self.session.data_objects.exists(source):
                source = self.session.data_objects.get(source)
                (diff, onlyFS, onlyIrods, same) = self.diffObjFile(source.path,
                                                                   os.path.join(
                                                                       destination, source.name),
                                                                   scope="checksum")
            elif self.session.collections.exists(source):
                source = self.session.collections.get(source)
                subdir = os.path.join(destination, source.name)
                if not os.path.isdir(os.path.join(destination, source.name)):
                    os.mkdir(os.path.join(destination, source.name))

                (diff, onlyFS, onlyIrods, same) = self.diffIrodsLocalfs(
                    source, subdir, scope="checksum")
            else:
                raise FileNotFoundError("ERROR iRODS download: {} not a valid source path".format(source))
        else:
            (diff, onlyFS, onlyIrods, same) = diffs

        if not force:  # Check space on destination
            try:
                space = disk_usage(destination).free
                if int(size) > (int(space) - buff):
                    logging.info('DOWNLOAD ERROR: Not enough space on disk.',
                                 exc_info=True)
                    raise ValueError('ERROR iRODS download: Not enough space on disk.')
                if buff < 0:
                    logging.info('DOWNLOAD ERROR: Negative disk buffer.', exc_info=True)
                    raise BufferError('ERROR iRODS download: Negative disk buffer.')
            except Exception as error:
                logging.info('DOWNLOAD ERROR', exc_info=True)
                raise error

        if self.session.data_objects.exists(source.path) and len(diff + onlyIrods) > 0:
            try:
                logging.info("IRODS DOWNLOADING object:" + source.path +
                             "to " + destination)
                #source_name = os.path.basename(source)
                self.session.data_objects.get(source.path,
                                              local_path=os.path.join(destination, source.name), **options)
                return
            except:
                logging.info('DOWNLOAD ERROR: ' + source.path + "-->" + destination,
                             exc_info=True)
                raise

        try:  # collections/folders
            #source_name = os.path.basename(source)
            subdir = os.path.join(destination, source.name)
            logging.info("IRODS DOWNLOAD started:")
            for d in diff:
                # upload files to distinct data objects
                logging.info("REPLACE: " + d[1] + " with " + d[0])
                self.session.data_objects.get(d[0], local_path=d[1], **options)

            for IO in onlyIrods:  # can contain files and folders
                # Create subcollections and upload
                sourcePath = source.path + "/" + IO
                locO = IO.replace("/", os.sep)
                destPath = os.path.join(subdir, locO)
                if not os.path.isdir(os.path.dirname(destPath)):
                    os.makedirs(os.path.dirname(destPath))
                logging.info('INFO: Downloading ' + sourcePath + " to " + destPath)
                self.session.data_objects.get(sourcePath, local_path=destPath, **options)
        except:
            logging.info('DOWNLOAD ERROR', exc_info=True)
            raise

    def diffObjFile(self, objPath, fsPath, scope="size"):
        """
        Compares and iRODS object to a file system file.
        returns ([diff], [onlyIrods], [onlyFs], [same])
        """
        if os.path.isdir(fsPath) and not os.path.isfile(fsPath):
            raise IsADirectoryError("IRODS FS DIFF: file is a directory.")
        if self.session.collections.exists(objPath):
            raise IsADirectoryError("IRODS FS DIFF: object exists already as collection. " + objPath)

        if not os.path.isfile(fsPath) and self.session.data_objects.exists(objPath):
            return ([], [], [objPath], [])

        elif not self.session.data_objects.exists(objPath) and os.path.isfile(fsPath):
            return ([], [fsPath], [], [])

        # both, file and object exist
        obj = self.session.data_objects.get(objPath)
        if scope == "size":
            objSize = obj.size
            fSize = os.path.getsize(fsPath)
            if objSize != fSize:
                return ([(objPath, fsPath)], [], [], [])
            else:
                return ([], [], [], [(objPath, fsPath)])
        elif scope == "checksum":
            objCheck = obj.checksum
            if objCheck == None:
                try:
                    obj.chksum()
                    objCheck = obj.checksum
                except:
                    logging.info('No checksum for ' + obj.path)
                    return ([(objPath, fsPath)], [], [], [])
            if objCheck.startswith("sha2"):
                sha2Obj = b64decode(objCheck.split('sha2:')[1])
                with open(fsPath, "rb") as f:
                    stream = f.read()
                    sha2 = hashlib.sha256(stream).digest()
                print(sha2Obj != sha2)
                if sha2Obj != sha2:
                    return ([(objPath, fsPath)], [], [], [])
                else:
                    return ([], [], [], [(objPath, fsPath)])
            elif objCheck:
                # md5
                with open(fsPath, "rb") as f:
                    stream = f.read()
                    md5 = hashlib.md5(stream).hexdigest()
                if objCheck != md5:
                    return ([(objPath, fsPath)], [], [], [])
                else:
                    return ([], [], [], [(objPath, fsPath)])

    def diffIrodsLocalfs(self, coll, dirPath, scope="size"):
        '''
        Compares and iRODS tree to a directory and lists files that are not in sync.
        Syncing scope can be 'size' or 'checksum'
        Returns: zip([dataObjects][files]) where ther is a difference
        collection: iRODS collection
        '''

        listDir = []
        if not dirPath == None:
            if not os.access(dirPath, os.R_OK):
                raise PermissionError("IRODS FS DIFF: No rights to write to destination.")
            if not os.path.isdir(dirPath):
                raise IsADirectoryError("IRODS FS DIFF: directory is a file.")
            for root, dirs, files in os.walk(dirPath, topdown=False):
                for name in files:
                    listDir.append(os.path.join(root.split(dirPath)[1], name).strip(os.sep))

        listColl = []
        if coll is not None:
            for root, subcolls, obj in coll.walk():
                for o in obj:
                    listColl.append(os.path.join(root.path.split(coll.path)[1], o.name).strip('/'))

        diff = []
        same = []
        for locPartialPath in set(listDir).intersection(listColl):
            iPartialPath = locPartialPath.replace(os.sep, "/")
            if scope == "size":
                objSize = self.session.data_objects.get(coll.path + '/' + iPartialPath).size
                fSize = os.path.getsize(os.path.join(dirPath, iPartialPath))
                if objSize != fSize:
                    diff.append((coll.path + '/' + iPartialPath, os.path.join(dirPath, locPartialPath)))
                else:
                    same.append((coll.path + '/' + iPartialPath, os.path.join(dirPath, locPartialPath)))
            elif scope == "checksum":
                objCheck = self.session.data_objects.get(coll.path + '/' + iPartialPath).checksum
                if objCheck == None:
                    try:
                        self.session.data_objects.get(coll.path + '/' + iPartialPath).chksum()
                        objCheck = self.session.data_objects.get(
                            coll.path + '/' + iPartialPath).checksum
                    except:
                        logging.info('No checksum for ' + coll.path + '/' + iPartialPath)
                        diff.append((coll.path + '/' + iPartialPath,
                                     os.path.join(dirPath, locPartialPath)))
                        continue
                if objCheck.startswith("sha2"):
                    sha2Obj = b64decode(objCheck.split('sha2:')[1])
                    with open(os.path.join(dirPath, locPartialPath), "rb") as f:
                        stream = f.read()
                        sha2 = hashlib.sha256(stream).digest()
                    if sha2Obj != sha2:
                        diff.append((coll.path + '/' + iPartialPath, os.path.join(dirPath, locPartialPath)))
                    else:
                        same.append((coll.path + '/' + iPartialPath, os.path.join(dirPath, locPartialPath)))
                elif objCheck:
                    # md5
                    with open(os.path.join(dirPath, locPartialPath), "rb") as f:
                        stream = f.read()
                        md5 = hashlib.md5(stream).hexdigest();
                    if objCheck != md5:
                        diff.append((coll.path + '/' + iPartialPath, os.path.join(dirPath, locPartialPath)))
                    else:
                        same.append((coll.path + '/' + iPartialPath, os.path.join(dirPath, locPartialPath)))
            else:  # same paths, no scope
                diff.append((coll.path + '/' + iPartialPath, os.path.join(dirPath, locPartialPath)))

        # adding files that are not on iRODS, only present on local FS
        # adding files that are not on local FS, only present in iRODS
        # adding files that are stored on both devices with the same checksum/size
        irodsOnly = list(set(listColl).difference(listDir))
        for i in range(0, len(irodsOnly)):
            irodsOnly[i] = irodsOnly[i].replace(os.sep, "/")
        return (diff, list(set(listDir).difference(listColl)), irodsOnly, same)

    def addMetadata(self, items, key, value, units=None):
        """
        Adds metadata to all items
        items: list of iRODS data objects or iRODS collections
        key: string
        value: string
        units: (optional) string 

        Throws:
            CATALOG_ALREADY_HAS_ITEM_BY_THAT_NAME
        """
        for item in items:
            try:
                item.metadata.add(key.upper(), value, units)
            except CATALOG_ALREADY_HAS_ITEM_BY_THAT_NAME:
                print(RED + "INFO ADD META: Metadata already present" + DEFAULT)
            except CAT_NO_ACCESS_PERMISSION:
                raise CAT_NO_ACCESS_PERMISSION("ERROR UPDATE META: no permissions")

    def updateMetadata(self, items, key, value, units=None):
        """
        Updates a metadata entry to all items
        items: list of iRODS data objects or iRODS collections
        key: string
        value: string
        units: (optional) string

        Throws: CAT_NO_ACCESS_PERMISSION
        """
        try:
            for item in items:
                if key in item.metadata.keys():
                    meta = item.metadata.get_all(key)
                    valuesUnits = [(m.value, m.units) for m in meta]
                    if (value, units) not in valuesUnits:
                        # remove all iCAT entries with that key
                        for m in meta:
                            item.metadata.remove(m)
                        # add key, value, units
                        self.addMetadata(items, key, value, units)

                else:
                    self.addMetadata(items, key, value, units)
        except CAT_NO_ACCESS_PERMISSION:
            raise CAT_NO_ACCESS_PERMISSION("ERROR UPDATE META: no permissions " + item.path)

    def deleteMetadata(self, items, key, value, units):
        """
        Deletes a metadata entry of all items
        items: list of iRODS data objects or iRODS collections
        key: string
        value: string
        units: (optional) string

        Throws:
            CAT_SUCCESS_BUT_WITH_NO_INFO: metadata did not exist
        """
        for item in items:
            try:
                item.metadata.remove(key, value, units)
            except CAT_SUCCESS_BUT_WITH_NO_INFO:
                print(RED + "INFO DELETE META: Metadata never existed" + DEFAULT)
            except CAT_NO_ACCESS_PERMISSION:
                raise CAT_NO_ACCESS_PERMISSION("ERROR UPDATE META: no permissions " + item.path)

    def deleteData(self, item):
        """
        Delete a data object or a collection recursively.
        item: iRODS data object or collection
        """

        if self.session.collections.exists(item.path):
            logging.info("IRODS DELETE: " + item.path)
            try:
                item.remove(recurse=True, force=True)
            except CAT_NO_ACCESS_PERMISSION:
                raise CAT_NO_ACCESS_PERMISSION("ERROR IRODS DELETE: no permissions")
        elif self.session.data_objects.exists(item.path):
            logging.info("IRODS DELETE: " + item.path)
            try:
                item.unlink(force=True)
            except CAT_NO_ACCESS_PERMISSION:
                raise CAT_NO_ACCESS_PERMISSION("ERROR IRODS DELETE: no permissions " + item.path)

    def executeRule(self, ruleFile, params, output='ruleExecOut'):
        """
        Executes and interactive rule. Returns stdout and stderr.
        params: Depending on rule,
                dictionary of variables for rule, will overwrite the default settings.
        params format example:
        params = {  # extra quotes for string literals
            '*obj': '"/zone/home/user"',
            '*name': '"attr_name"',
            '*value': '"attr_value"'
        }
        """
        try:
            rule = Rule(self.session, ruleFile, params=params, output=output)
            out = rule.execute()
        except Exception as e:
            logging.info('RULE EXECUTION ERROR', exc_info=True)
            return [], [repr(e)]

        stdout = []
        stderr = []
        if len(out.MsParam_PI) > 0:
            try:
                stdout = [o.decode()
                          for o in (out.MsParam_PI[0].inOutStruct.stdoutBuf.buf.strip(b'\x00')).split(b'\n')]
                stderr = [o.decode()
                          for o in (out.MsParam_PI[0].inOutStruct.stderrBuf.buf.strip(b'\x00')).split(b'\n')]
            except AttributeError:
                logging.info('RULE EXECUTION ERROR: ' + str(stdout + stderr), exc_info=True)
                return stdout, stderr

        return stdout, stderr

    def getSize(self, itemPaths, ticket_string=None):
        '''
        Compute the size of the iRods dataobject or collection
        Returns: size in bytes.
        itemPaths: list of irods paths pointing to collection or object
        '''
        if ticket_string:
            logging.debug("Using the ticket {}".format(ticket_string))
            Ticket(self.session, ticket_string).supply()

        size = 0
        for path in itemPaths:
            # remove possible leftovers of windows fs separators
            path = path.replace("\\", "/")
            if self.session.data_objects.exists(path):
                size = size + self.session.data_objects.get(path).size

            elif self.session.collections.exists(path):
                coll = self.session.collections.get(path)
                walk = [coll]
                while walk:
                    try:
                        coll = walk.pop()
                        walk.extend(coll.subcollections)
                        for obj in coll.data_objects:
                            size = size + obj.size
                    except:
                        logging.info('DATA SIZE', exc_info=True)
                        raise
        return size

    def createTicket(self, path, expiryInterval=None):
        ticket = Ticket(self.session,
                        ''.join(random.choice(string.ascii_letters) for _ in range(20)))
        restore_acl = False
        if not self.isAccessible(target_path=path, access_type='own'):
            self.getAccess(target_path=path, access_type='own')
            restore_acl = True
        ticket.issue("read", path)
        expSetFlag = False
        if expiryInterval:
            # iRODS tickets ignore the server time zone, iRODS server assumes the timestamp zone being UTC
            epoch_seconds = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc).timestamp()
            delay = lambda secs: int(epoch_seconds + secs + 1)
            ticket.modify('expire', delay(expiryInterval))
            expSetFlag = True
        logging.info('CREATE TICKET: ' + ticket.ticket + ': ' + path + ': ' + str(expiryInterval))
        if restore_acl:
            self.setPermissions(rights='null', user=self.session.username, path=path, zone=self.session.zone,
                                recursive=True)
        # returns False when no expiry date is set
        return ticket.ticket, expSetFlag

    def copy_coll(self, source_path, target_path):

        source_coll = self.session.collections.get(source_path)
        for collection, subcollections, data_objects in source_coll.walk():

            current_target = iRODSPath(target_path + '/' + source_coll.name)
            logging.debug("Creating the collection {}".format(current_target))
            self.session.collections.create(current_target, recurse=True)
            for data_obj in data_objects:
                options = {kw.VERIFY_CHKSUM_KW: '', kw.FORCE_FLAG_KW: ''}
                logging.debug("Copying the object {} to {}".format(data_obj.path, current_target))
                self.session.data_objects.copy(data_obj.path, current_target, **options)

    # it copies irods objects and collections
    def copyData(self, source, destination):

        if not self.session.collections.exists(destination):
            try:
                self.session.collections.create(destination)
            except CAT_NO_ACCESS_PERMISSION:
                logging.error('No access permission on target collection path {}'.format(destination))
                raise

        norm_path = iRODSPath(source)
        try:
            options = {kw.VERIFY_CHKSUM_KW: '', kw.FORCE_FLAG_KW: ''}
            if self.session.data_objects.exists(norm_path):
                self.session.data_objects.copy(norm_path, destination, **options)
            elif self.session.collections.exists(norm_path):
                self.copy_coll(norm_path, destination)
            else:
                logging.error('Source path {} does not exists'.format(norm_path))
                raise
        except CAT_NO_ACCESS_PERMISSION:
            logging.error('No access permission on source path {}'.format(norm_path))
            raise

    def close(self):
        try:
            self.session.cleanup()
        except NetworkException as e:
            logging.error('Network exception on iRODS session clean up: {}'.format(e))

    def isAccessible(self, target_path, access_type=None):
        if not access_type:
            access_type = 'read'
        matching_dict = {'read': [self.acl_mapping['read'], self.acl_mapping['write'], self.acl_mapping['own']],
                         'write': [self.acl_mapping['write'], self.acl_mapping['own']],
                         'own': [self.acl_mapping['own']]
                         }
        perms = self.getPermissions(target_path)
        logging.debug("The target path {} has permissions {}".format(target_path, perms))
        user_info = self.getUserInfo()
        return (len([ac for ac in perms if ac.access_name in matching_dict[access_type]
                     and (ac.user_name == self.session.username
                          or ac.user_name in user_info[1])]) > 0)

    def getAccess(self, target_path, access_type):
        userTypes, groups = self.getUserInfo()
        adminFlag = True if userTypes.pop() == 'rodsadmin' else False
        try:
            acl0 = iRODSAccess(self.acl_mapping[access_type], target_path, self.session.username, self.session.zone)
            if self.session.collections.exists(target_path):
                self.session.acls.set(acl0, admin=adminFlag, recursive=True)
                acl1 = iRODSAccess('inherit', target_path)
                self.session.acls.set(acl1, admin=adminFlag)
            else:
                self.session.acls.set(acl0, admin=adminFlag, recursive=False)
        except CAT_NO_ACCESS_PERMISSION:
            logging.error('No access permission on path {}'.format(target_path))
            raise

    def getObjChecksum(self, path, compute=False):
        obj = self.session.data_objects.get(path)
        objCheck = obj.checksum
        if objCheck == None:
            if compute:
                try:
                    objCheck = self.session.data_objects.chksum(obj.path, **{kw.FORCE_CHKSUM_KW:''})
                except:
                    logging.info('No checksum for ' + obj.path)
                    return None, None
            else:
                return None, None
        if objCheck.startswith("sha2"):
            return "sha256", (b64decode(objCheck.split('sha2:')[1])).hex()
        else:
            return "md5", objCheck

    def getChecksum(self, path, compute=False):
        chk_dict = {}
        if self.session.data_objects.exists(path):
            chk_type, chk_sum = self.getObjChecksum(path=path, compute=compute)
            chk_dict = {path: {'type': chk_type, 'sum': chk_sum}}
        elif self.session.collections.exists(path):
            coll = self.session.collections.get(path)
            walk = [coll]
            while walk:
                try:
                    coll = walk.pop()
                    walk.extend(coll.subcollections)
                    for obj in coll.data_objects:
                        chk_type, chk_sum = self.getObjChecksum(path=obj.path, compute=compute)
                        chk_dict[obj.path] = {'type': chk_type, 'sum': chk_sum}
                except:
                    logging.info('DATA CHECKSUM', exc_info=True)
                    raise
        return chk_dict
