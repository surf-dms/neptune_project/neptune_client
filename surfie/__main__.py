import getpass
import json
import logging
import logging.handlers
import os
import uuid
import warnings
import dateparser
from datetime import timedelta, datetime
from urllib.parse import urlparse

import click
import typer

from irods.access import iRODSAccess

from rich.console import Console
from rich.table import Table
from rich.tree import Tree
from rich.panel import Panel
from rich.rule import Rule
from rich import print
from enum import Enum
from types import SimpleNamespace
from pathlib import Path
from typing import Optional
from typing import List
from typing import Tuple
from typing_extensions import Annotated

from surfie.irodswave import irodsConnector
from surfie.irodswave.intorods import intorods
from surfie.neptunewave import neptune

from surfie import helpers
from surfie import neptuneSession
from surfie import surfieAdmin

from openapi_client.models.provenance_action_type import ProvenanceActionType

warnings.filterwarnings("ignore", category=UserWarning)

log = logging.getLogger(__name__)
config_dict = {}
neptune = neptune.Neptune()

app = typer.Typer()
session_app = typer.Typer()
app.add_typer(session_app, name="session")
app.add_typer(surfieAdmin.admin_app, name="admin")


def init_surfie():
    neptune.set_log(log)

    if config_dict and len(config_dict) > 0:
        if 'url' in config_dict['default'].keys():
            neptune.host = config_dict['default']['url']
        if 'ca_cert_file' in config_dict['default'].keys():
            neptune.set_ssl_params(verify_ssl=True, ssl_ca_cert=config_dict['default']['ca_cert_file'])
        if 'username' in config_dict['default'].keys():
            neptune.username = config_dict['default']['username']
        if 'password' in config_dict['default'].keys():
            neptune.passwd = config_dict['default']['password']
    if "NEPTUNE_URL" in os.environ:
        neptune.host = os.environ["NEPTUNE_URL"]
    if "NEPTUNE_CA_CERT_FILE" in os.environ:
        neptune.set_ssl_params(verify_ssl=True, ssl_ca_cert=os.environ["NEPTUNE_CA_CERT_FILE"])
    if "NEPTUNE_USERNAME" in os.environ:
        neptune.username = os.environ["NEPTUNE_USERNAME"]
    if "NEPTUNE_PASSWORD" in os.environ:
        neptune.passwd = os.environ["NEPTUNE_PASSWORD"]
    if not neptune.host:
        neptune.host = input("Please enter neptune url: ")
    if not neptune.configuration.verify_ssl or not neptune.configuration.ssl_ca_cert:
        neptune.set_ssl_params(verify_ssl=False, ssl_ca_cert=None)
    if not neptune.username:
        neptune.username = input("Please enter your username: ")
    if not neptune.passwd:
        pw = getpass.getpass().encode()
        neptune.passwd = pw.decode()
    log.debug("surfie's ssl verify: {} and ssl ca cert: {}".format(neptune.configuration.verify_ssl,
                                                                   neptune.configuration.ssl_ca_cert))
    neptune.set_configuration()
    neptune.set_api_client()
    parse_url = urlparse(neptune.host)
    hostname, port = parse_url.netloc.split(":")
    neptune.providers = neptune.list_providers()
    provider = neptune.get_provider_by_address(hostname=hostname, port=int(port))
    if provider:
        neptune.friendly_name = provider.friendly_name
    else:
        typer.echo("Provider Neptune not found with address {}:{}".format(hostname,str(port)))
        raise typer.Exit(0)


class SessionStates(str, Enum):
    active = "active"
    expired = "expired"
    suspended = 'suspended'
    resumed = 'resumed'


class SessionAccessType(str, Enum):
    private = "private"
    shared = "shared"
    public = 'public'


@session_app.callback()
def session_callback(ctx: typer.Context,
                     code: Optional[uuid.UUID] = typer.Option(None, "--code", "-c", help="session code"),
                     json_filter: Annotated[
                         str, typer.Option("--json", help="filter by json clause {key: value}")] = None,
                     text_filter: Annotated[str, typer.Option("--text", help="filter by text clause")] = None,
                     status: Annotated[SessionStates, typer.Option(case_sensitive=False)] = SessionStates.active,
                     access: Annotated[SessionAccessType, typer.Option(case_sensitive=False)] = None,
                     time_interval: Annotated[str, typer.Option("--delta", help="time interval ('2 days' or '06:30:55')")] = None,
                     author: Annotated[str, typer.Option("--author", help="show results from a specific user")] = None
                     ):

    time_delta = None
    delta_string = None
    if time_interval:
        delta = datetime.now() - dateparser.parse(time_interval)
        time_delta = round(delta.total_seconds())
        delta_string = str(delta)
    session_db_list = neptune.get_sessions(code, json_search=json_filter, text_search=text_filter, status=status,
                                           access=access, time_interval=delta_string, author=author)
    if code:
        if len(session_db_list) == 0:
            typer.echo("Session {} not found".format(code))
            raise typer.Exit(0)
        else:
            log.debug("Session {} found".format(code))

    options = {"sessions": session_db_list, "code": code, "time_interval": time_delta}
    ctx.obj = SimpleNamespace(options=options)


@session_app.command("share")
def session_share(ctx: typer.Context, paths: list[str],
                  public: Optional[bool] = typer.Option(False, "--public", "-p", help="public session"),
                  shared: Annotated[str, typer.Option("--with", help="shared session with participants "
                                                                     "(comma separated list with no spaces)")] = None,
                  noaccess: Annotated[
                      bool, typer.Option("--noaccess", help="to share only metadata with no access to data")] = None,
                  meta: Annotated[
                      str, typer.Option("--meta", help="metadata (comma separated list with no spaces)")] = None):
    sessions = ctx.obj.options["sessions"]
    code = ctx.obj.options["code"]
    time_interval = ctx.obj.options["time_interval"]
    encryption_key = None

    if code:
        session = sessions[0]
    else:
        session = None

    shared_list = None
    if public:
        access = "public"
    elif shared:
        access = "shared"
        shared_list = shared.split(',')
    else:
        access = "private"

    neptune_session = neptuneSession.neptuneSession(neptune=neptune, config_dict=config_dict,
                                                    code=code, session=session,
                                                    session_type=access, shared_list=shared_list,
                                                    lifetime=time_interval)
    for path in paths:
        if neptune_session.is_data_path_remote(path):
            operation = "share"
            target_path = None
        else:
            # upload the data
            operation = "upload"
            irods_connector = neptuneSession.initialize_irods_session(config_dict, neptune.irods_friendly_name)
            target = '/' + irods_connector.session.zone + '/tmp/' + str(uuid.uuid4())
            log.debug("uploading {} to {}".format(path, target))
            size = 0
            if os.path.isfile(path):
                size = os.stat(path).st_size
            else:
                size = helpers.folder_size(path, size)
            irods_connector.uploadData(source=path, destination=target, size=size, resource=None, force=True)
            target_path = neptune.irods_friendly_name + '::' + target
            neptune_session.is_data_path_remote(target_path)
            irods_connector.close()

        try:
            neptune_session.add_system_metadata(path)
        except Exception as e:
            log.error("Cannot add system metadata for path {}: {}".format(path, e))
            raise typer.Exit(0)

        if meta:
            neptune_session.add_metadata(meta=meta, path=path)

        neptune_session.add_session_event(op=operation, path=path, target_path=target_path, noaccess=noaccess)
    neptune_session.finalize()
    typer.echo("session: {}".format(neptune_session.session.code))


@session_app.command("request")
def session_request(ctx: typer.Context, paths: list[str],
                    public: Optional[bool] = typer.Option(False, "--public", "-p", help="public session"),
                    shared: Annotated[str, typer.Option("--with", help="shared session with participants "
                                                                       "(comma separated list with no spaces)")] = None,
                    target: Annotated[str, typer.Option("--dest", help="the location where the data should be delivered")] = None,
                    by: Annotated[str, typer.Option("--by", help="the data user on behalf of who the request is done")] = None,
                    bymeta: Annotated[str, typer.Option("--bymeta", help="the metadata related to the data user")] = None):
    sessions = ctx.obj.options["sessions"]
    code = ctx.obj.options["code"]
    time_interval = ctx.obj.options["time_interval"]

    if code:
        session = sessions[0]
    else:
        session = None

    shared_list = None
    if public:
        access = "public"
    elif shared:
        access = "shared"
        shared_list = shared.split(',')
    else:
        access = "private"

    neptune_session = neptuneSession.neptuneSession(neptune=neptune, config_dict=config_dict,
                                                    code=code, session=session,
                                                    session_type=access, shared_list=shared_list,
                                                    lifetime=time_interval)
    neptune_session.set_log(log)
    for path in paths:
        if neptune_session.is_data_path_remote(path):
            if target:
                if not neptune_session.is_data_path_remote(target):
                    typer.echo(
                        "Request to copy data to a local folder, a remote provider is expected: no request issued")
                    typer.Exit(0)
            operation = 'request'
            if by:
                neptune_session.is_data_path_remote(path=by)
            if bymeta:
                neptune_session.add_metadata(meta=bymeta, path=by)
            neptune_session.add_session_event(op=operation, path=path, target_path=target, noaccess=True, user=by)
            neptune_session.finalize()
            typer.echo("session: {}".format(neptune_session.session.code))
        else:
            typer.echo("Request for local data, a remote provider is expected: no request issued")


@session_app.command("list")
def session_list(ctx: typer.Context,
                 verbose: Annotated[bool, typer.Option("--v", help="verbose output")] = None,
                 veryverbose: Annotated[bool, typer.Option("--vv", help="very verbose output")] = None,
                 js: Annotated[bool, typer.Option("--json", help="print json output")] = None,
                 approve: Annotated[bool, typer.Option("--approve", help="requests approval")] = None,
                 operation: Annotated[helpers.OperationType, typer.Option("--type")] = None):
    sessions = ctx.obj.options["sessions"]
    verbose = True if veryverbose else verbose
    if sessions and len(sessions) > 0:
        verbose = True if (approve and operation and operation == helpers.OperationType.request) else verbose
        console = Console()
        session_table = Table("Code", "Status", "Access", "Created", "Owner")
        if not js:
            typer.echo("sessions:")
        for session_db in sessions:

            show, show_approval, request_target = helpers.session_view_by_operation(operation=operation,
                                                                                    session=session_db)
            if show:
                if js:
                    typer.echo(json.dumps(session_db.to_dict(), indent=4, sort_keys=True, default=str))
                else:
                    session_user = neptune.get_user(session_db.data_user)
                    session_username = session_user.username if session_user else 'unknown'
                    if not verbose:
                        session_table.add_row(session_db.code, session_db.status, session_db.access,
                                              str(session_db.create_ts), session_username, style="bold")
                    else:
                        helpers.session_detailed_view(session=session_db, session_username=session_username,
                                                      neptune=neptune, veryverbose=veryverbose)

                if show_approval:
                    reviewer_list, session_table = helpers.session_view_provenance(provenance=session_db.provenance,
                                                                                   session_table=session_table,
                                                                                   verbose=verbose, js=js)
                    if approve:
                        request_approval = not (neptune.get_username() in reviewer_list)
                        helpers.session_view_request_approval(session=session_db, neptune=neptune,
                                                              request_approval=request_approval,
                                                              request_target=request_target)
                    if (not js) and verbose:
                        console.rule()
        if session_table.rows:
            console.print(session_table, overflow='fold')
    else:
        typer.echo("Session not found")
        raise typer.Exit(0)


@session_app.command("reshare")
def session_reshare(ctx: typer.Context,
                    public: Optional[bool] = typer.Option(False, "--public", "-p", help="public session"),
                    shared: Annotated[str, typer.Option("--with", help="shared session with participants "
                                                                       "(comma separated list with no spaces)")] = None,
                    operation: Annotated[helpers.OperationType, typer.Option("--type")] = None,
                    source: Annotated[str, typer.Option("--from", help="data providers "
                                                                       "(comma separated list with no spaces)")] = None,
                    target: Annotated[str, typer.Option("--to", help="data providers "
                                                                     "(comma separated list with no spaces)")] = None):
    sessions = ctx.obj.options["sessions"]
    time_interval = ctx.obj.options["time_interval"]

    for provider in [source, target]:
        if provider and not(provider in [p.friendly_name for p in neptune.providers]):
            typer.echo("The data provider {} is not one of the registered providers".format(provider))
            raise typer.Exit(0)

    shared_list = None
    if public:
        access = "public"
    elif shared:
        access = "shared"
        shared_list = shared.split(',')
    else:
        access = "private"

    for session in sessions:
        # do not list a session already re-shared
        username = neptune.get_username()
        if helpers.session_provenance_lookup(session=session, neptune=neptune, op=ProvenanceActionType.SHARED,
                                             user_name=username):
            continue

        # filter events based on operation, source and target
        events_list = neptune.get_session_events(session=session, operation=operation, source=source, target=target)

        if len(events_list) > 0:
            share_session = neptuneSession.neptuneSession(neptune=neptune, config_dict=config_dict,
                                                          code=None, session=None,
                                                          session_type=access, shared_list=shared_list,
                                                          lifetime=time_interval)
            # copy events to the new session replacing the operation request with share
            share_session.import_events(events_list=events_list, operation=helpers.OperationType.share)
            share_session.finalize()
            typer.echo("session: {}".format(share_session.session.code))
            # add provenance record on the original request
            neptune.add_signature(session=session, action=ProvenanceActionType.SHARED, rel_sess_code=share_session.session.code)


@session_app.command("download")
def session_download(ctx: typer.Context,
                     target: Annotated[str, typer.Option("--to", help="target local or remote destination")] = None,
                     encryption_key: Optional[str] = typer.Option(None, "--encryption", "-e", help="encryption key"),
                     preserve_tree: Annotated[bool, typer.Option("--pt", help="preserve the source tree structure")] = None):

    sessions = ctx.obj.options["sessions"]
    for session in sessions:
        # do not list a session already checked for copying/downloading
        username = neptune.get_username()
        if helpers.session_provenance_lookup(session=session, neptune=neptune, op=ProvenanceActionType.COPIED,
                                             user_name=username):
            continue

        neptune_session = neptuneSession.neptuneSession(neptune=neptune, config_dict=config_dict, session=session,
                                                        code=session.code, session_type=session.access)
        if neptune_session.download_data_by_operations(operations=["share","upload"],
                                                       target=target,
                                                       encryption_key=encryption_key,
                                                       preserve_source_tree=preserve_tree):
            neptune.add_signature(session=session, action=ProvenanceActionType.COPIED)


@session_app.command("acknowledge")
def session_acknowledge(ctx: typer.Context):

    sessions = ctx.obj.options["sessions"]
    for session in sessions:
        # do not list a session already acknowledged
        if helpers.session_provenance_lookup(session=session, neptune=neptune, op=ProvenanceActionType.COPIED,
                                             user_name=neptune.get_username()):
            continue
        if helpers.session_provenance_lookup(session=session, neptune=neptune, op=ProvenanceActionType.SHARED,
                                             user_name=neptune.get_username()):
            for signature in session.provenance:
                action, username, timestamp, rel_sess_code = helpers.getSignatureAttributes(signature)
                if action == ProvenanceActionType.SHARED and username == neptune.get_username() and rel_sess_code:
                    rel_sess = neptune.get_sessions(code=rel_sess_code).pop()
                    for rel_signature in rel_sess.provenance:
                        action, username, timestamp, rel_sess_code = helpers.getSignatureAttributes(rel_signature)
                        if action == ProvenanceActionType.COPIED:
                            neptune.add_signature(session=session, action=action, rel_sess_code=rel_sess.code)
                            typer.echo("added signature to session {}: action {} related to session {}".format(
                                session.code, action, rel_sess.code))


@app.callback(invoke_without_command=True)
def main(ctx: typer.Context,
         conf: Optional[Path] = typer.Option(None, "--conf", "-c", help="conf file path"),
         verbose: bool = typer.Option(False, "--verbose", "-v"),
         quiet: bool = typer.Option(False, "--quiet", "-q")
         ):
    """
    run with values mentioned in config file
    or set the values with provided keys at command prompt
    """
    if conf:
        # Parse configuration file
        globals()['config_dict'] = helpers.parse_configuration(conf)
    # Override conf file with commandline options
    if verbose & quiet:
        typer.echo('please choose either "verbose" or "quiet" ')
        raise typer.Exit(0)
    if verbose:
        log.setLevel(logging.DEBUG)
    elif quiet:
        log.setLevel(logging.WARNING)
    else:
        log.setLevel(logging.INFO)

    formatter = logging.Formatter('%(levelname)s %(asctime)s: %(message)s', "%Y-%m-%d %H:%M:%S")
    if 'log_file' in config_dict.keys():
        fh = logging.handlers.RotatingFileHandler(config_dict['log_file'], maxBytes=10000000, backupCount=5)
        fh.setFormatter(formatter)
        log.addHandler(fh)
    else:
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        log.addHandler(ch)

    log.debug("Configuration: {}".format(config_dict))
    init_surfie()
    ctx.obj = SimpleNamespace(neptune=neptune)


def main_app():
    app()

if __name__ == "__main__":
    main_app()
