import builtins
import os
import pprint
import time
import uuid
import pytest
import shlex
import shutil
import json
import requests
import uvicorn

from surfie import __main__
from surfie import helpers
from typer.testing import CliRunner
from multiprocessing import Process
from waves.main import app


runner = CliRunner()

# def run_waves(host, port):
#     uvicorn.run(app, host=host, port=port)
#
#
# @pytest.fixture(autouse=True, scope="session")
# def waves():
#     proc = Process(target=run_waves, args=("localhost", 8080), daemon=True)
#     proc.start()
#     time.sleep(2)
#     yield
#     proc.kill() # Cleanup after test


# def test_connect_to_waves(waves):
#     response = requests.get("http://localhost:8080/docs")
#     assert response.status_code == 200


# Fixture to get user command-line arguments
@pytest.fixture
def get_user_input(request):
    conf = str(request.config.getoption("--conf"))
    meta = str(request.config.getoption("--meta"))
    return conf, meta


def share_data(conf, metadata, data_path, user=None, public=None, noaccess=None):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["session", "share"]
    if public:
        input_list = input_list + ["--public"]
    if noaccess:
        input_list = input_list + ["--noaccess"]
    input_list = input_list + shlex.split("--meta " + metadata)
    if user:
        input_list = input_list + shlex.split("--with " + user)
    input_list = input_list + [data_path]
    return runner.invoke(__main__.app, input_list)


def add_user(conf, user, password, role=None):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["admin", "datauser"]
    user_role = role if role else "user"
    input_list = input_list + shlex.split("add --role " + user_role + " " + user + " " + password)
    return runner.invoke(__main__.app, input_list)


def remove_user(conf, user):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["admin", "datauser", "remove", user]
    return runner.invoke(__main__.app, input_list)


def download_data(user, password, uuid, destination=None, preserve_source_tree=False):
    os.environ['NEPTUNE_USERNAME'] = user
    os.environ['NEPTUNE_PASSWORD'] = password
    input_list = ["session"]
    input_list = input_list + shlex.split("--code " + uuid)
    input_list = input_list + ["download"]
    if destination:
        input_list = input_list + shlex.split("--to " + destination)
    if preserve_source_tree:
        input_list = input_list + shlex.split("--pt")
    result = runner.invoke(__main__.app, input_list)
    os.environ.pop('NEPTUNE_USERNAME')
    os.environ.pop('NEPTUNE_PASSWORD')
    return result


def request_data(conf, data_path, data_destination=None, user_path=None, user_meta=None, user=None):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["session", "request"]
    if data_destination:
        input_list = input_list + shlex.split("--dest " + data_destination)
    if user_path:
        input_list = input_list + shlex.split("--by " + user_path)
        if user_meta:
            input_list = input_list + shlex.split("--bymeta " + user_meta)
    if user:
        input_list = input_list + shlex.split("--with " + user)
    input_list = input_list + [data_path]
    return runner.invoke(__main__.app, input_list)


def approve_request(user, password, uuid):
    os.environ['NEPTUNE_USERNAME'] = user
    os.environ['NEPTUNE_PASSWORD'] = password
    input_list = ["session"]
    input_list = input_list + shlex.split("--code " + uuid)
    input_list = input_list + ["list", "--v"]
    input_list = input_list + shlex.split("--type " + "request")
    input_list = input_list + shlex.split("--approve")
    result = runner.invoke(__main__.app, input_list, input="y\n")
    os.environ.pop('NEPTUNE_USERNAME')
    os.environ.pop('NEPTUNE_PASSWORD')
    return result


def list_data(conf, session_code=None, json_output=False):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["session"]
    if session_code:
        input_list = input_list + shlex.split("--code " + str(session_code))
    input_list = input_list + ["list"]
    if json_output:
        input_list = input_list + ["--json"]
    return runner.invoke(__main__.app, input_list)


def reshare_data(conf, user=None, public=None, source=None, target=None, operation=None):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["session", "reshare"]
    if public:
        input_list = input_list + ["--public"]
    if user:
        input_list = input_list + shlex.split("--with " + user)
    if source:
        input_list = input_list + shlex.split("--from " + source)
    if target:
        input_list = input_list + shlex.split("--to " + target)
    if operation:
        input_list = input_list + shlex.split("--type " + operation)
    return runner.invoke(__main__.app, input_list)


def acknowledge_copy(conf):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["session", "acknowledge"]
    return runner.invoke(__main__.app, input_list)


def project_creation(conf, name, duration):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["admin", "project", "add", name, duration]
    return runner.invoke(__main__.app, input_list)


def project_set_service_accounts(conf, name, service_accounts):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["admin", "project", "set", name]
    if service_accounts:
        input_list = input_list + shlex.split("service_accounts " + service_accounts)
    return runner.invoke(__main__.app, input_list)


def list_project(conf, name=None, verbose=False, json=False):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["admin", "project", "list"]
    if name:
        input_list = input_list + shlex.split("--name " + name)
    if verbose:
        input_list = input_list + shlex.split("--v")
    if json:
        input_list = input_list + shlex.split("--json")
    return runner.invoke(__main__.app, input_list)


def list_providers(conf, name=None, json=False):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["admin", "provider", "list"]
    if name:
        input_list = input_list + shlex.split("--name " + name)
    if json:
        input_list = input_list + shlex.split("--json")
    return runner.invoke(__main__.app, input_list)


def list_datausers(conf, provider_name=None, json=False):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["admin", "datauser", "list"]
    if provider_name:
        input_list = input_list + shlex.split("--provider " + provider_name)
    if json:
        input_list = input_list + shlex.split("--json")
    return runner.invoke(__main__.app, input_list)


def remove_project(conf, provider_name):
    input_list = shlex.split("--conf " + conf)
    input_list = input_list + ["admin", "project", "remove"]
    input_list = input_list + shlex.split("--name " + provider_name)
    return runner.invoke(__main__.app, input_list)


def test_public_sharing_of_local_data(capsys, get_user_input):
    data_local_path = 'client_tests/inputs/helloWorld.txt'
    local_target_folder = 'client_tests/outputs'
    user = 'Guybrush'
    password = 'grog'

    conf, meta = get_user_input
    # share a local file with metadata
    result = share_data(conf=conf, metadata=meta, data_path=data_local_path, public=True)
    share_data_response = capsys.readouterr()
    for line in share_data_response.out.strip().splitlines():
        if "session" in line:
            uuid_string = (line.replace("session: ", "")).strip()
            uuid_object = uuid.UUID(uuid_string)
        else:
            continue
    assert result.exit_code == 0
    assert "session: " in share_data_response.out.strip()
    assert uuid_object is not None

    # add a user
    result = add_user(conf=conf, user=user, password=password)
    add_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "Created data user: {}".format(user) in add_user_response.out

    # download a file
    result = download_data(user=user, password=password, uuid=uuid_string, destination=local_target_folder)
    assert result.exit_code == 0
    file_name = os.path.basename(data_local_path)
    downloaded_path = local_target_folder + os.path.sep + file_name
    assert os.path.isfile(downloaded_path)

    # remove the file
    os.remove(downloaded_path)

    # remove the user
    result = remove_user(conf=conf, user=user)
    remove_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "data user {} deleted".format(user) in remove_user_response.out.strip()


def test_restricted_sharing_of_remote_data(capsys, get_user_input):
    data_remote_path = '/surf/home/public/testdir'
    local_target_folder = "client_tests/outputs"
    data_provider = 'lighthouse'
    user = 'Guybrush'
    password = 'grog'

    conf, meta = get_user_input
    # add a user
    result = add_user(conf=conf, user=user, password=password)
    add_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "Created data user: {}".format(user) in add_user_response.out

    # share remote data with metadata
    dp = data_provider + "::" + data_remote_path
    result = share_data(conf=conf, metadata=meta, data_path=dp, user=user)
    share_data_response = capsys.readouterr()
    for line in share_data_response.out.strip().splitlines():
        if "session" in line:
            uuid_string = (line.replace("session: ", "")).strip()
            uuid_object = uuid.UUID(uuid_string)
        else:
            continue
    assert result.exit_code == 0
    assert "session: " in share_data_response.out
    assert uuid_object is not None

    # download the data
    result = download_data(user=user, password=password, uuid=uuid_string, destination=local_target_folder)
    assert result.exit_code == 0
    data_name = os.path.basename(data_remote_path)
    downloaded_path = local_target_folder + os.path.sep + data_name
    assert os.path.isdir(downloaded_path)

    # remove the data
    shutil.rmtree(downloaded_path)

    # remove the user
    result = remove_user(conf=conf, user=user)
    remove_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "data user {} deleted".format(user) in remove_user_response.out


def test_restricted_sharing_with_no_data_access(capsys, get_user_input):
    data_remote_path = '/surf/home/neptune/HelloIRODS.txt'
    metadata_remote_path = '/surf/home/neptune/irods.metadata.json'
    data_provider = 'lighthouse'
    user = 'Guybrush'
    password = 'grog'
    conf, meta = get_user_input

    # add a user
    result = add_user(conf=conf, user=user, password=password)
    add_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "Created data user: {}".format(user) in add_user_response.out

    # share remote data with metadata
    dp = data_provider + "::" + data_remote_path
    json_meta = "json::{}".format(metadata_remote_path)
    result = share_data(conf=conf, metadata=json_meta, data_path=dp, user=user, noaccess=True)
    share_data_response = capsys.readouterr()
    for line in share_data_response.out.splitlines():
        if "session" in line:
            uuid_string = (line.replace("session: ", "")).strip()
            uuid_object = uuid.UUID(uuid_string)
        else:
            continue
    assert result.exit_code == 0
    assert "session: " in share_data_response.out
    assert uuid_object is not None

    # check that the credentials are null
    json_result = list_data(conf, session_code=uuid_object, json_output=True)
    list_data_response = capsys.readouterr()
    assert json_result.exit_code == 0
    session_dict = json.loads(list_data_response.out)
    ticket_is_not_null = False
    for event in session_dict['events']:
        if event['ticket']:
            ticket_is_not_null = True
            break
    assert ticket_is_not_null == False

    # remove the user
    result = remove_user(conf=conf, user=user)
    remove_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "data user {} deleted".format(user) in remove_user_response.out


def test_project_creation(capsys, get_user_input):
    conf, meta = get_user_input
    provider = 'provider1'
    provider_passwd = 'monkey'
    name = 'Project9'
    duration = '1'
    service_accounts = "provider1::lighthouse"
    # add a provider service account
    result = add_user(conf=conf, user=provider, password=provider_passwd, role='provider')
    add_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "Created data user: {}".format(provider) in add_user_response.out
    # create the project
    result = project_creation(conf=conf, name=name, duration=duration)
    project_creation_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "Created project: {}".format(name) in project_creation_response.out
    result = project_set_service_accounts(conf=conf, name=name, service_accounts=service_accounts)
    assert result.exit_code == 0


def test_restricted_request_and_approval(capsys, get_user_input):
    project_name = 'Project9'
    data_remote_path = '/surf/home/public/testdir'
    data_destination = '/surf/home/neptune'
    data_provider = 'lighthouse'
    identity_provider = 'sram'
    user = 'Guybrush.Threepwood'
    user_meta = 'json::client_tests/inputs/user_meta.json'
    reviewer = 'LeChuck'
    reviewer_passwd = 'grog'
    conf, meta = get_user_input

    # add a user
    result = add_user(conf=conf, user=reviewer, password=reviewer_passwd, role='reviewer')
    add_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "Created data user: {}".format(reviewer) in add_user_response.out

    # get the project's mapping between service accounts and providers
    json_result_project = list_project(conf=conf, name=project_name, json=True)
    list_project_response = capsys.readouterr()
    assert json_result_project.exit_code == 0
    json_result_provider = list_providers(conf=conf, name=data_provider, json=True)
    list_providers_response = capsys.readouterr()
    assert json_result_provider.exit_code == 0
    json_result_datauser = list_datausers(conf, provider_name='neptune', json=True)
    list_datausers_response = capsys.readouterr()
    assert json_result_datauser.exit_code == 0
    project_dict = json.loads(list_project_response.out)
    provider_dict = json.loads(list_providers_response.out)
    datausers_dict = json.loads(list_datausers_response.out)
    datauser_map = {}
    for datauser_dict in datausers_dict['neptune']:
        datauser_map[datauser_dict['_id']] = datauser_dict['username']
    for service_account_id, provider_id in project_dict['service_accounts'].items():
        if provider_id == provider_dict['_id']:
            service_account_name = datauser_map[service_account_id]
            break
    # request remote data
    dp = data_provider + "::" + data_remote_path
    dd = data_provider + "::" + data_destination
    user_path = identity_provider + '::' + user
    users = reviewer + ',' + service_account_name
    result = request_data(conf=conf, data_path=dp, data_destination=dd, user_path=user_path,
                          user_meta=user_meta, user=users)
    request_data_response = capsys.readouterr()
    for line in request_data_response.out.splitlines():
        if "session" in line:
            uuid_string = (line.replace("session: ", "")).strip()
            uuid_object = uuid.UUID(uuid_string)
        else:
            continue
    assert result.exit_code == 0
    assert "session: " in request_data_response.out
    assert uuid_object is not None

    # approve the request
    result = approve_request(user=reviewer, password=reviewer_passwd, uuid=uuid_string)
    approve_request_response = capsys.readouterr()
    assert result.exit_code == 0

    # check that signature of the reviewer is in the provenance list
    json_result = list_data(conf, session_code=uuid_object, json_output=True)
    list_data_response = capsys.readouterr()
    assert json_result.exit_code == 0
    session_dict = json.loads(list_data_response.out)
    assert len(session_dict['provenance']) == 1
    signature = session_dict['provenance'][0]
    action, username, timestamp = signature.split('::')
    assert action == 'approved' and username == reviewer

    # check that user metadata are added to the profile
    user_metadata_found = False
    for profile in session_dict['profiles']:
        if 'metadata' in profile.keys():
            if 'user' in profile['metadata'].keys():
                user_metadata_found = True
                assert profile['metadata']['user']['project'] == "treasure hunt"
    assert user_metadata_found == True

    # remove the user
    result = remove_user(conf=conf, user=reviewer)
    remove_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "data user {} deleted".format(reviewer) in remove_user_response.out
    # remove the provider account
    result = remove_user(conf=conf, user=service_account_name)
    remove_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "data user {} deleted".format(service_account_name) in remove_user_response.out
    # remove the project
    result = remove_project(conf, project_name)
    remove_project_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "Removed {}".format(project_name) in remove_project_response.out


def test_restricted_resharing_of_remote_data(capsys, get_user_input):
    data_provider = 'lighthouse'
    data_provider_user = 'provider1'
    target_provider = 'lighthouse'
    target_provider_user = 'provider2'
    target_provider_passwd = 'island'

    conf, meta = get_user_input
    # add a user
    result = add_user(conf=conf, user=target_provider_user, password=target_provider_passwd, role='provider')
    add_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "Created data user: {}".format(target_provider_user) in add_user_response.out
    # reshare data
    result = reshare_data(conf=conf, user=target_provider_user, source=data_provider, target=target_provider,
                          operation=helpers.OperationType.request)
    reshare_data_response = capsys.readouterr()
    for line in reshare_data_response.out.splitlines():
        if "session" in line:
            uuid_string = (line.replace("session: ", "")).strip()
            uuid_object = uuid.UUID(uuid_string)
        else:
            continue
    assert result.exit_code == 0
    assert "session: " in reshare_data_response.out
    assert uuid_object is not None

    # download the shared data
    result = download_data(user=target_provider_user, password=target_provider_passwd, uuid=uuid_string,
                           preserve_source_tree=True)
    download_data_response = capsys.readouterr()
    assert result.exit_code == 0

    # remove the target provider user
    result = remove_user(conf=conf, user=target_provider_user)
    remove_user_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "data user {} deleted".format(target_provider_user) in remove_user_response.out


def test_acknowledge(capsys, get_user_input):
    conf, meta = get_user_input
    result = acknowledge_copy(conf=conf)
    acknowledge_copy_response = capsys.readouterr()
    assert result.exit_code == 0
    assert "added signature to session" in acknowledge_copy_response.out
