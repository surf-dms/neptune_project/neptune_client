def pytest_addoption(parser):
    parser.addoption("--conf", action="store", default="../config.real.ini", help="conf file")
    parser.addoption("--meta", action="store", default="'just metadata'", help="metadata")
