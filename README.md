# SURFIE

The CLI client of the Neptune platform.  
Neptune, the god of the sea, controls the (data)waves.  
The client "wave" to control an iRODS (https://irods.org) data repository is based on 
 - iBridges (https://github.com/UtrechtUniversity/iBridges).
 - intorods (https://github.com/rivm-syso/intorods/).  

The project uses poetry (https://python-poetry.org).  
Documentation: [docs](docs/overview.md).

## Getting started

It has been tested only with python 3.10 and 3.11.
```commandline
pip install git+https://gitlab.com/surf-dms/neptune_project/neptune_api_client.git@devel
pip install git+https://gitlab.com/surf-dms/neptune_project/neptune_client.git@devel
```

## Configuration

There is a configuration file template: see config.ini.  

## Usage
For example, to list the available data providers:  
```commandline
surfie --conf config.ini admin provider list
```
Or to list the sessions:  
```commandline
surfie --conf config.ini session list
```
For the list of the available options:  
```commandline
surfie --help
```
